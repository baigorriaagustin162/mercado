package paquetemercado.SectorTest;

import org.junit.Assert;
import org.junit.Test;
import paquetemercado.Sector.*;

public class PuestoTest {

    @Test
    public void testPuestosIguales(){
        boolean confirmar;
        Puesto puesto11 = new Puesto("11", 10, false, true, 10000, true);
        Puesto puesto5 = new Puesto("11", 7, false, true, 10000, true);
        confirmar = puesto11.equals(puesto5); //intento instanciar 2 sectores con igual ubicacion, devuelve true
        Assert.assertEquals(true,confirmar); 
    }

    @Test
    public void testPuestosDistintos(){
        boolean confirmar;
        Puesto puesto11 = new Puesto("11", 10, false, true, 10000, true);
        Puesto puesto5 = new Puesto("5", 7, false, true, 10000, true);
        confirmar = puesto11.equals(puesto5); //intento instanciar 2 sectores con distinta ubicacion, devuelve false
        Assert.assertEquals(false,confirmar); 
    }
}
