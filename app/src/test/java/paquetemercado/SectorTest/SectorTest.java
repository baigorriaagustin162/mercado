package paquetemercado.SectorTest;

import java.util.ArrayList;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import paquetemercado.Sector.*;
import paquetemercado.Sector.ExceptionsSector.*;

public class SectorTest {
    @Test
    public void sectoresIguales(){
        boolean confirmar;
        Sector sectorA = new Sector("Zona norte");
        Sector sectorB = new Sector("Zona norte");
        confirmar = sectorA.equals(sectorB); //intento instanciar 2 sectores iguales, devuelve true
        Assert.assertEquals(true,confirmar); 
    }

    @Test
    public void sectoresDistintos(){
        boolean confirmar;
        Sector sectorA = new Sector("Zona norte");
        Sector sectorB = new Sector("Zona sur");
        confirmar = sectorA.equals(sectorB); //intento instanciar 2 sectores distintos, devuelve false
        Assert.assertEquals(false,confirmar); 
    }

    @Test
    public void agregarPuestoASector() throws PuestoYaExisteEnSector{
        Puesto puesto1 = new Puesto("1",14,true,false,15000,true);
        Sector sectorA = new Sector("Zona norte");
        sectorA.agregarPuesto(puesto1); //se agrega puesto1 con ubicacion 1
        Puesto puesto3 = new Puesto("3",12,false,false,10000,true);
        sectorA.agregarPuesto(puesto3); //se agrega puesto3 con ubicacion 3
        ArrayList<Puesto> auxPuestos = (sectorA.getPuestos());
        Assert.assertTrue(auxPuestos.contains(puesto1));
        Assert.assertTrue(auxPuestos.contains(puesto3));
    }

    @Test
    public void testEliminarPuestoDeSector() throws PuestoYaExisteEnSector, NoExistePuestoConEsaUbicacionEnElSector{
        Puesto puesto1 = new Puesto("1",14,true,false,15000,true);
        Puesto puesto3 = new Puesto("3",12,false,false,10000,true);
        Sector sectorA = new Sector("ZONA NORTE");
        sectorA.agregarPuesto(puesto1); //se agrega puesto1 a sectorA
        sectorA.agregarPuesto(puesto3); //se agrega puesto3 a SectorA
        sectorA.eliminarPuesto(puesto3); //se elimina puesto3 del sectorA
        ArrayList<Puesto> auxPuestos = (sectorA.getPuestos());
        Assert.assertTrue(auxPuestos.contains(puesto1));
        Assert.assertFalse(auxPuestos.contains(puesto3));
    }

    @Test
    public void getPuestoEnSectorSegunUbicacion() throws PuestoYaExisteEnSector, PuestoNoEncontradoException{
        Puesto puesto1 = new Puesto("1",14,true,false,15000,true);
        Sector sectorA = new Sector("ZONA NORTE");
        sectorA.agregarPuesto(puesto1); //se agrega puesto1 a sectorA
        Optional<Puesto> auxPuesto = sectorA.getPuestoEnSectorSegunUbicacion("1");
        Puesto puestoDevuelto = auxPuesto.get();
        Assert.assertTrue(puestoDevuelto.equals(puesto1));
    }

    @Test
    public void getPuestosDisponibles() throws PuestoYaExisteEnSector, NoHayPuestosDisponiblesEnElSector{
        Puesto puesto1 = new Puesto("1",14,true,false,15000,true);
        Puesto puesto3 = new Puesto("3",12,false,false,10000,false);
        Sector sectorA = new Sector("ZONA NORTE");
        sectorA.agregarPuesto(puesto1);
        sectorA.agregarPuesto(puesto3);
        ArrayList<Puesto> auxPuestos = sectorA.getPuestosDisponibles();
        Assert.assertTrue(auxPuestos.contains(puesto1));
        Assert.assertFalse(auxPuestos.contains(puesto3));
    }

    @Test
    public void getPuestosConPrecioMenorOIgualAParametro() throws PuestoYaExisteEnSector, NoHayPuestosConPrecioMenorAEse{
        Puesto puesto1 = new Puesto("1",14,true,false,15000,true);
        Puesto puesto3 = new Puesto("3",12,false,false,10000,false);
        Sector sectorA = new Sector("ZONA NORTE");
        sectorA.agregarPuesto(puesto1);
        sectorA.agregarPuesto(puesto3);
        ArrayList<Puesto> auxPuestos = sectorA.getPuestosConPrecioMenorOIgualAParametro(13000.2);
        Assert.assertFalse(auxPuestos.contains(puesto1));
        Assert.assertTrue(auxPuestos.contains(puesto3));
    }

    @Test(expected = PuestoNoEncontradoException.class)
    public void puestoNoEncontradoException() throws PuestoYaExisteEnSector, PuestoNoEncontradoException{
        Puesto puesto1 = new Puesto("1",14,true,false,15000,true);
        Sector sectorA = new Sector("ZONA NORTE");
        sectorA.agregarPuesto(puesto1);
        @SuppressWarnings("unused")
        Optional<Puesto> auxPuesto = sectorA.getPuestoEnSectorSegunUbicacion("5");
    }

    @Test (expected = PuestoYaExisteEnSector.class)
    public void puestoYaExisteEnSector() throws PuestoYaExisteEnSector{
        Sector sectorA = new Sector("ZONA NORTE");
        Puesto puesto1 = new Puesto("1",14,true,false,15000,true);
        Puesto puesto3 = new Puesto("1",12,false,false,10000,false);
        sectorA.agregarPuesto(puesto1);
        sectorA.agregarPuesto(puesto3);
    }

    @Test(expected = NoHayPuestosDisponiblesEnElSector.class)
    public void noHayPuestosDisponiblesEnElSector() throws PuestoYaExisteEnSector, NoHayPuestosDisponiblesEnElSector{
        Sector sectorA = new Sector("ZONA NORTE");
        Puesto puesto1 = new Puesto("1",14,true,false,15000,false);
        Puesto puesto3 = new Puesto("3",12,false,false,10000,false);
        sectorA.agregarPuesto(puesto1);
        sectorA.agregarPuesto(puesto3);
        @SuppressWarnings("unused")
        ArrayList<Puesto> auxPuestos = sectorA.getPuestosDisponibles();
    }

    @Test(expected = NoHayPuestosConPrecioMenorAEse.class)
    public void noHayPuestosConPrecioMenorAEse() throws PuestoYaExisteEnSector, NoHayPuestosConPrecioMenorAEse{
        Puesto puesto1 = new Puesto("1",14,true,false,15000,true);
        Puesto puesto3 = new Puesto("3",12,false,false,10000,false);
        Sector sectorA = new Sector("ZONA NORTE");
        sectorA.agregarPuesto(puesto1);
        sectorA.agregarPuesto(puesto3);
        @SuppressWarnings("unused")
        ArrayList<Puesto> auxPuestos = sectorA.getPuestosConPrecioMenorOIgualAParametro(8000.5);

    }

    @Test (expected = NoExistePuestoConEsaUbicacionEnElSector.class)
    public void noExistePuestoConEsaUbicacionEnElSector() throws PuestoYaExisteEnSector, NoExistePuestoConEsaUbicacionEnElSector{
        Puesto puesto1 = new Puesto("1",14,true,false,15000,true);
        Puesto puesto2 = new Puesto("3",14,true,false,15000,true);
        Sector sectorA = new Sector("ZONA NORTE");
        sectorA.agregarPuesto(puesto1);
        sectorA.eliminarPuesto(puesto2);
    }
}
