package paquetemercado.SectorTest;

import org.junit.Assert;
import org.junit.Test;
import java.util.ArrayList;
import paquetemercado.Sector.ExceptionsSector.*;
import paquetemercado.Sector.*;

public class SectorRepositorioTest {
    
    @Test
    public void ingresarSectorAlRepositorio() throws YaExisteElSectorEnElRepositorio, PuestoYaExisteEnSector {
        SectorRepositorio repo1 = new SectorRepositorio();
        Sector sector1 = new Sector("Zona1");
        Puesto puesto1 = new Puesto("Ubicacion1", 10.0, true, true, 1000.0, true);
        sector1.agregarPuesto(puesto1);
        repo1.ingresarSectorAlRepositorio(sector1);
        ArrayList<Sector> aux = repo1.getSectoresDelRepositorio();
        Assert.assertTrue(aux.contains(sector1));
    }

    @Test
    public void obtenerSectorPorZona() throws PuestoYaExisteEnSector, YaExisteElSectorEnElRepositorio, SectorNoEncontradoException {
        SectorRepositorio repo1 = new SectorRepositorio();
        Sector sector1 = new Sector("Zona1");
        Puesto puesto1 = new Puesto("Ubicacion1", 10.0, true, true, 1000.0, true);
        sector1.agregarPuesto(puesto1);
        repo1.ingresarSectorAlRepositorio(sector1);
        Sector sectorDevuelto = repo1.obtenerSectorPorZona("Zona1");
        Assert.assertEquals(sector1, sectorDevuelto);
    }

    @Test
    public void testActualizarSector() throws YaExisteElSectorEnElRepositorio, SectorNoEncontradoException{
        Sector sector1 = new Sector("ZonaActualizada");
        SectorRepositorio repositorio = new SectorRepositorio();
        repositorio.ingresarSectorAlRepositorio(sector1);
        repositorio.modificarUbicacion(sector1, "Zona1");
        Assert.assertEquals(sector1.getZonaSector(), "Zona1");
    }

    @Test
    public void eliminarSectorDelRepositorio() throws SectorNoEncontradoException, YaExisteElSectorEnElRepositorio {
        SectorRepositorio repositorio = new SectorRepositorio();
        Sector sector1 = new Sector("ZonaActualizada");
        repositorio.ingresarSectorAlRepositorio(sector1);
        repositorio.eliminarSectorDelRepositorio(sector1);
        ArrayList<Sector> aux = repositorio.getSectoresDelRepositorio();
        Assert.assertFalse(aux.contains(sector1));
    }

    @Test (expected = YaExisteElSectorEnElRepositorio.class)
    public void yaExisteElSectorEnElRepositorio() throws YaExisteElSectorEnElRepositorio{
        Sector sector1 = new Sector("ZonaActualizada");
        Sector sector2 = new Sector("ZonaActualizada");
        SectorRepositorio repositorio = new SectorRepositorio();
        repositorio.ingresarSectorAlRepositorio(sector1);
        repositorio.ingresarSectorAlRepositorio(sector2);
    }

    @Test (expected = SectorNoEncontradoException.class)
    public void sectorNoEncontradoException() throws YaExisteElSectorEnElRepositorio, SectorNoEncontradoException{
        SectorRepositorio repositorio = new SectorRepositorio();
        Sector sector1 = new Sector("ZonaActualizada");
        repositorio.ingresarSectorAlRepositorio(sector1);
        @SuppressWarnings("unused")
        Sector sector = repositorio.obtenerSectorPorZona("zona1");
    }
}
