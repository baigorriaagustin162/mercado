package paquetemercado.MedidorTest;

import paquetemercado.lectura.*;
import paquetemercado.lectura.ExcepcionesMedidorYLectura.*;
import java.time.LocalDate;
import org.junit.Test;
import org.junit.Assert;
import java.util.ArrayList;
import java.util.Optional;

public class RepositorioDeMedidoresTest {
    @Test
    public void ingresarMedidorAlRepositorio() throws ConsumoInvalido, CodigoInvalido, YaExisteMedidorEnRepositorio, NoHayLecturaEnEsteMedidor, YaExisteLecturaDelMesProporsionado, NoExisteMedidor{
        RepositorioDeMedidores repo1 = new RepositorioDeMedidores();
        LecturaMedidor lectura1 = new LecturaMedidor(LocalDate.of(2024, 10, 4), 242.3);
        Medidor puesto1 = new Medidor(62345);
        puesto1.agregarLecturaDelMedidor(lectura1);
        repo1.ingresarMedidorAlRepositorio(puesto1);
        ArrayList<Medidor> auxMedidor = (repo1.getMedidores());
        Assert.assertTrue(auxMedidor.contains(puesto1));
    }

    @Test (expected = YaExisteMedidorEnRepositorio.class)
    public void yaExisteMedidorEnRepositorio() throws ConsumoInvalido, CodigoInvalido, NoHayLecturaEnEsteMedidor, YaExisteLecturaDelMesProporsionado, YaExisteMedidorEnRepositorio{
        RepositorioDeMedidores repo1 = new RepositorioDeMedidores();
        LecturaMedidor lectura1 = new LecturaMedidor(LocalDate.of(2024, 10, 4), 242.3);
        Medidor puesto1 = new Medidor(62345);
        //agregando medidor1 al repo
        puesto1.agregarLecturaDelMedidor(lectura1);
        repo1.ingresarMedidorAlRepositorio(puesto1);
        //defino otro medidor con el mismo codigo e intento subirlo al repo
        Medidor puesto2 = new Medidor(62345);
        LecturaMedidor lectura11 = new LecturaMedidor(LocalDate.of(2024, 10, 4), 242.3);
        puesto2.agregarLecturaDelMedidor(lectura11);
        repo1.ingresarMedidorAlRepositorio(puesto2);
    }

    @Test
    public void eliminarMedidorDelRepositorio() throws ConsumoInvalido, CodigoInvalido, YaExisteMedidorEnRepositorio, NoHayLecturaEnEsteMedidor, YaExisteLecturaDelMesProporsionado, NoExisteMedidor{
        RepositorioDeMedidores repo1 = new RepositorioDeMedidores();
        LecturaMedidor lectura1 = new LecturaMedidor(LocalDate.of(2024, 10, 4), 242.3);
        Medidor puesto1 = new Medidor(62345);
        puesto1.agregarLecturaDelMedidor(lectura1);
        repo1.ingresarMedidorAlRepositorio(puesto1);
        repo1.eliminarMedidorDelRepositorio(puesto1);
        ArrayList<Medidor> auxMedidor = (repo1.getMedidores());
        Assert.assertFalse(auxMedidor.contains(puesto1));
    }

    @Test (expected = NoExisteMedidor.class)
    public void noExisteMedidor() throws ConsumoInvalido, CodigoInvalido, NoHayLecturaEnEsteMedidor, YaExisteLecturaDelMesProporsionado, NoExisteMedidor{
        RepositorioDeMedidores repo1 = new RepositorioDeMedidores();
        LecturaMedidor lectura1 = new LecturaMedidor(LocalDate.of(2024, 10, 4), 242.3);
        Medidor puesto1 = new Medidor(62345);
        puesto1.agregarLecturaDelMedidor(lectura1);
        Medidor puesto2 = new Medidor(23666);
        puesto2.agregarLecturaDelMedidor(lectura1);
        //intento eliminar el medidor 2 que no esta en el repo
        repo1.eliminarMedidorDelRepositorio(puesto2);
    }

    @Test 
    public void devolverMedidorSegunCodigoMedidor() throws ConsumoInvalido, CodigoInvalido, NoHayLecturaEnEsteMedidor, YaExisteLecturaDelMesProporsionado, YaExisteMedidorEnRepositorio, NoExisteMedidor{
        RepositorioDeMedidores repo1 = new RepositorioDeMedidores();
        LecturaMedidor lectura1 = new LecturaMedidor(LocalDate.of(2024, 10, 4), 242.3);
        Medidor puesto1 = new Medidor(62345);
        puesto1.agregarLecturaDelMedidor(lectura1);
        repo1.ingresarMedidorAlRepositorio(puesto1);
        Optional<Medidor> opcional = repo1.getMedidorSegunCodigoMedidor(62345);
        Medidor medidor = opcional.get();
        Assert.assertTrue(puesto1.equals(medidor));
    }


    @Test(expected = NoExisteMedidor.class)
    public void noExisteMedidorConEseCodigo() throws ConsumoInvalido, CodigoInvalido, NoHayLecturaEnEsteMedidor, YaExisteLecturaDelMesProporsionado, NoExisteMedidor, YaExisteMedidorEnRepositorio{
        RepositorioDeMedidores repo1 = new RepositorioDeMedidores();
        LecturaMedidor lectura1 = new LecturaMedidor(LocalDate.of(2024, 10, 4), 242.3);
        Medidor puesto1 = new Medidor(62345);
        puesto1.agregarLecturaDelMedidor(lectura1);
        repo1.ingresarMedidorAlRepositorio(puesto1);
        @SuppressWarnings("unused")
        Optional<Medidor> opcional = repo1.getMedidorSegunCodigoMedidor(12333);
    }
}
