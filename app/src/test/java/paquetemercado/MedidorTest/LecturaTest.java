package paquetemercado.MedidorTest;

import paquetemercado.lectura.*;
import paquetemercado.lectura.ExcepcionesMedidorYLectura.ConsumoInvalido;
import java.time.LocalDate;
import org.junit.Test;
import org.junit.Assert;

public class LecturaTest {
    @Test
    public void lecturasDatosIguales() throws ConsumoInvalido {
        boolean confirmar;
        LecturaMedidor lectura1 = new LecturaMedidor(LocalDate.of(2024, 10, 4), 242.3);
        LecturaMedidor lectura2 = new LecturaMedidor(LocalDate.of(2024, 10, 4), 242.3);
        confirmar = lectura1.equals(lectura2); // devuelve true porque tienen la misma fecha
        Assert.assertEquals(true, confirmar);
    }

    @Test
    public void lecturasFechasDistintas() throws ConsumoInvalido {
        boolean confirmar;
        LecturaMedidor lectura1 = new LecturaMedidor(LocalDate.of(2024, 10, 4), 242.3);
        LecturaMedidor lectura2 = new LecturaMedidor(LocalDate.of(2024, 11, 4), 242.3);
        confirmar = lectura1.equals(lectura2); // devuelve false porque tienen distinta fecha, por mas que el consumo
                                               // sea igual
        Assert.assertEquals(false, confirmar);
    }

    @Test
    public void lecturasFechasDistintasYConsumoIgual() throws ConsumoInvalido{
        boolean confirmar;
        LecturaMedidor lectura1 = new LecturaMedidor(LocalDate.of(2024, 10, 4), 242.3);
        LecturaMedidor lectura2 = new LecturaMedidor(LocalDate.of(2024, 11, 4), 200.3);
        confirmar = lectura1.equals(lectura2); // devuelve false porque tienen distinta fecha, por mas que el consumo
                                               // sea igual
        Assert.assertEquals(false, confirmar);
    }

    @Test
    public void lecturasConDatosDistintos() throws ConsumoInvalido{
        boolean confirmar;
        LecturaMedidor lectura1 = new LecturaMedidor(LocalDate.of(2024, 10, 4), 242.3);
        LecturaMedidor lectura2 = new LecturaMedidor(LocalDate.of(2024, 11, 4), 200.3);
        confirmar = lectura1.equals(lectura2); // devuelve false porque tienen distinta fecha, por mas que el consumo
                                               // sea igual
        Assert.assertEquals(false, confirmar);
    }

    @Test (expected = ConsumoInvalido.class)
    public void consumoInvalido() throws ConsumoInvalido{
        @SuppressWarnings("unused")
        LecturaMedidor lectura1 = new LecturaMedidor(LocalDate.of(2024, 10, 4), -123);
        
    }

}
