package paquetemercado.MedidorTest;

import paquetemercado.lectura.*;
import paquetemercado.lectura.ExcepcionesMedidorYLectura.*;
import java.time.LocalDate;
import org.junit.Test;
import org.junit.Assert;
import java.util.ArrayList;
import java.util.Optional;

public class MedidorTest {

    @Test
    public void agregarNuevaLecturaAMedidor() throws ConsumoInvalido, CodigoInvalido, NoHayLecturaEnEsteMedidor, YaExisteLecturaDelMesProporsionado {
        boolean auxConfirmacion;
        LecturaMedidor lectura1 = new LecturaMedidor(LocalDate.of(2024, 10, 4), 242.3);
        Medidor puesto1 = new Medidor(62345);
        puesto1.agregarLecturaDelMedidor(lectura1);
        auxConfirmacion = puesto1.buscarSiExisteLecturaEnMedidor(lectura1);
        Assert.assertEquals(true, auxConfirmacion);
    }

    @Test
    public void eliminarLecturaDeMedidor() throws CodigoInvalido, NoHayLecturaEnEsteMedidor, YaExisteLecturaDelMesProporsionado, ConsumoInvalido {
        boolean auxConfirmacion;
        LecturaMedidor lectura1 = new LecturaMedidor(LocalDate.of(2024, 10, 4), 242.3);
        Medidor puesto1 = new Medidor(62345);
        puesto1.agregarLecturaDelMedidor(lectura1);
        puesto1.eliminarUnaLecturaDelMedidor(lectura1);
        auxConfirmacion = puesto1.buscarSiExisteLecturaEnMedidor(lectura1); //devuelve falso porq se elimino lect
        Assert.assertFalse(auxConfirmacion);
    }

    @Test
    public void igualdadDeMedidores() throws CodigoInvalido {
        boolean confirmar;
        Medidor medidor1 = new Medidor(4353);
        Medidor medidor2 = new Medidor(4353);
        confirmar = medidor1.equals(medidor2); // devuelve true porque tienen el mismo codigo de Medidor
        Assert.assertEquals(true, confirmar);
    }

    @Test
    public void distintosMedidores() throws CodigoInvalido {
        boolean confirmar;
        Medidor medidor1 = new Medidor(4353);
        Medidor medidor3 = new Medidor(8237);
        confirmar = medidor1.equals(medidor3); // devuelve false porque tienen distinto codigoMedidor
        Assert.assertEquals(false, confirmar);
    }

    @Test(expected = CodigoInvalido.class)
    public void ingresarCodigoDeMedidor() throws CodigoInvalido{
        @SuppressWarnings("unused")
        Medidor medidor1 = new Medidor(-32465);
    }

    @Test(expected = YaExisteLecturaDelMesProporsionado.class)
    public void ingresarDosLecturasEnElMismoMes() throws ConsumoInvalido, CodigoInvalido, NoHayLecturaEnEsteMedidor, YaExisteLecturaDelMesProporsionado{
        Medidor medidor3 = new Medidor(8237);
        LecturaMedidor lectura1 = new LecturaMedidor(LocalDate.of(2024,5,6), 180.2);
        LecturaMedidor lectura2 = new LecturaMedidor(LocalDate.of(2024,5,18), 180.2);
        medidor3.agregarLecturaDelMedidor(lectura1);
        medidor3.agregarLecturaDelMedidor(lectura2);
    }

    @Test
    public void devolverLecturaSegunFecha() throws ConsumoInvalido, CodigoInvalido, NoHayLecturaEnEsteMedidor, YaExisteLecturaDelMesProporsionado, NoHayLecturaConEsaFechaEnEsteMedidor{
        LecturaMedidor lectura1 = new LecturaMedidor(LocalDate.of(2024, 10, 4), 242.3);
        Medidor puesto1 = new Medidor(62345);
        puesto1.agregarLecturaDelMedidor(lectura1);
        Optional<LecturaMedidor> opcional = puesto1.getLecturaMedidorSegunFecha(LocalDate.of(2024, 10, 4));
        LecturaMedidor auxLectura = opcional.get();
        Assert.assertTrue(auxLectura.equals(lectura1));
    }

    @Test(expected = NoHayLecturaEnEsteMedidor.class)
    public void NoHayLecturaEnEsteMedidor() throws CodigoInvalido, NoHayLecturaEnEsteMedidor, ConsumoInvalido{
        LecturaMedidor lectura1 = new LecturaMedidor(LocalDate.of(2024, 10, 4), 242.3);
        Medidor puesto1 = new Medidor(62345);
        puesto1.eliminarUnaLecturaDelMedidor(lectura1);
    }

    @Test(expected = NoHayLecturaConEsaFechaEnEsteMedidor.class)
    public void NoHayLecturaConEsaFechaEnEsteMedidor() throws ConsumoInvalido, CodigoInvalido, NoHayLecturaEnEsteMedidor, YaExisteLecturaDelMesProporsionado, NoHayLecturaConEsaFechaEnEsteMedidor{
        LecturaMedidor lectura1 = new LecturaMedidor(LocalDate.of(2024, 10, 4), 242.3);
        Medidor puesto1 = new Medidor(62345);
        puesto1.agregarLecturaDelMedidor(lectura1);
        Optional<LecturaMedidor> opcional = puesto1.getLecturaMedidorSegunFecha(LocalDate.of(2024, 1, 4));
        LecturaMedidor auxLectura = opcional.get();
        Assert.assertFalse(auxLectura.equals(lectura1));
    }

    @Test
    public void existeLecturaEnMedidor() throws CodigoInvalido, ConsumoInvalido, NoHayLecturaEnEsteMedidor, YaExisteLecturaDelMesProporsionado{
        Medidor medidor3 = new Medidor(8237);
        LecturaMedidor lectura1 = new LecturaMedidor(LocalDate.of(2024,5,6), 180.2);
        medidor3.agregarLecturaDelMedidor(lectura1);
        boolean existe = medidor3.buscarSiExisteLecturaEnMedidor(lectura1);
        Assert.assertTrue(existe);
    }

    @Test
    public void devolverLecturasDelMedidor() throws CodigoInvalido, ConsumoInvalido, NoHayLecturaEnEsteMedidor, YaExisteLecturaDelMesProporsionado{
        Medidor medidor3 = new Medidor(8237);
        LecturaMedidor lectura1 = new LecturaMedidor(LocalDate.of(2024,5,6), 180.2);
        LecturaMedidor lectura2 = new LecturaMedidor(LocalDate.of(2024,6,10), 150.8);
        medidor3.agregarLecturaDelMedidor(lectura1);
        medidor3.agregarLecturaDelMedidor(lectura2);
        ArrayList<LecturaMedidor> auxLecturas = medidor3.getLecturasDelMedidor();
        Assert.assertTrue(auxLecturas.contains(lectura1));
        Assert.assertTrue(auxLecturas.contains(lectura2));
    }

    @Test
    public void noExisteEstaLecturaEnElRepositorio() throws CodigoInvalido, ConsumoInvalido, NoHayLecturaEnEsteMedidor, YaExisteLecturaDelMesProporsionado{
            Medidor medidor3 = new Medidor(8237);
            LecturaMedidor lectura1 = new LecturaMedidor(LocalDate.of(2024,5,6), 180.2);
            boolean existe = medidor3.buscarSiExisteLecturaEnMedidor(lectura1);
            Assert.assertFalse(existe);
    }
}