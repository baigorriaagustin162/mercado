package paquetemercado.ClienteTest;

import org.junit.Assert;
import org.junit.Test;

import paquetemercado.cliente.Quintero;

public class QuinteroTest {
    @Test 
    public void testEmpresasIguales(){
        boolean confirmar;
        Quintero quintero1 = new Quintero("274834309", "Maria","Flores", "Mascotas");
        Quintero quintero2 = new Quintero("274834309", "Sofia","Torreira", "Mascotas");
        confirmar = quintero1.equals(quintero2); // devuelve true porque tienen el mismo cuit, se consideran iguales
        Assert.assertEquals(true, confirmar);
    }

    @Test
    public void testEmpresasDistintas(){
        boolean confirmar;
        Quintero quintero1 = new Quintero("272374449", "Maria","Flores", "Mascotas");
        Quintero quintero2 = new Quintero("274834309", "Sofia","Torreira", "Mascotas");
        confirmar = quintero1.equals(quintero2); // devuelve false porque tienen distinto cuit, se consideran distintas
        Assert.assertEquals(false, confirmar);
    }
}
