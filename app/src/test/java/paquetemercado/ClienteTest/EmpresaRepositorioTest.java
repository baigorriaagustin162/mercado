package paquetemercado.ClienteTest;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import paquetemercado.cliente.Empresa;
import paquetemercado.cliente.EmpresaRepositorio;
import paquetemercado.cliente.ExceptionsCliente.EmpresaNoEncontradaException;
import paquetemercado.cliente.ExceptionsCliente.EmpresaYaExisteException;

public class EmpresaRepositorioTest {
    private EmpresaRepositorio repositorio;
    private Empresa empresa1;

    @Before
    public void setUp() {
        repositorio = new EmpresaRepositorio();
        empresa1 = new Empresa("20304050607", "Empresa Uno", "Rubro Uno", "Razón Social Uno");
    }

    @Test
    public void agregarEmpresa() throws EmpresaYaExisteException {
        repositorio.agregar(empresa1);
        assertEquals(1, repositorio.obtenerTodas().size());
        assertEquals(empresa1, repositorio.obtenerTodas().get(0));
    }

    @Test
    public void agregarEmpresaYaExiste() {
        try {
            repositorio.agregar(empresa1);
            repositorio.agregar(empresa1);
            fail("Se esperaba una EmpresaYaExisteException");
        } catch (EmpresaYaExisteException e) {
            assertEquals("La empresa ya existe en la lista", e.getMessage());
        }
    }

    @Test
    public void obtenerPorCuit() throws EmpresaNoEncontradaException, EmpresaYaExisteException {
        repositorio.agregar(empresa1);
        Empresa empresaEncontrada = repositorio.obtenerPorCuit(empresa1.getCuit());
        assertEquals(empresa1, empresaEncontrada);
    }

    @Test
    public void obtenerPorCuitNoEncontrado() {
        try {
            repositorio.obtenerPorCuit("99999999999");
            fail("Se esperaba una EmpresaNoEncontradaException");
        } catch (EmpresaNoEncontradaException e) {
            assertEquals("No se encontró una empresa con el CUIT especificado.", e.getMessage());
        }
    }

    @Test
    public void actualizarEmpresa() throws EmpresaNoEncontradaException, EmpresaYaExisteException {
        repositorio.agregar(empresa1);
        Empresa empresaActualizada = new Empresa("20304050607", "Empresa Uno Actualizada", "Nuevo Rubro",
                "Nueva Razón Social");
        repositorio.actualizar(empresa1.getCuit(), empresaActualizada);
        assertEquals(empresaActualizada, repositorio.obtenerPorCuit(empresa1.getCuit()));
    }

    @Test
    public void actualizarEmpresaNoEncontrada() {
        try {
            Empresa empresaActualizada = new Empresa("99999999999", "Empresa Actualizada", "Nuevo Rubro",
                    "Nueva Razón Social");
            repositorio.actualizar("99999999999", empresaActualizada);
            fail("Se esperaba una EmpresaNoEncontradaException");
        } catch (EmpresaNoEncontradaException e) {
            assertEquals("No se encontró una empresa con el CUIT especificado.", e.getMessage());
        }
    }

    @Test
    public void eliminarEmpresa() throws EmpresaNoEncontradaException, EmpresaYaExisteException {
        repositorio.agregar(empresa1);
        repositorio.eliminar(empresa1.getCuit());
        assertEquals(0, repositorio.obtenerTodas().size());
    }

    @Test
    public void eliminarEmpresaNoEncontrada() {
        try {
            repositorio.eliminar("99999999999");
            fail("Se esperaba una EmpresaNoEncontradaException");
        } catch (EmpresaNoEncontradaException e) {
            assertEquals("No se encontró una empresa con el CUIT especificado.", e.getMessage());
        }
    }
}
