package paquetemercado.ClienteTest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import paquetemercado.cliente.ExceptionsCliente.QuinteroNoEncontradoException;
import paquetemercado.cliente.ExceptionsCliente.QuinteroYaExisteException;
import paquetemercado.cliente.Quintero;
import paquetemercado.cliente.QuinteroRepositorio;;

public class QuinteroRepositorioTest {
    private QuinteroRepositorio repositorio;
    private Quintero quintero1;

    @Before
    public void setUp() {
        repositorio = new QuinteroRepositorio();
        quintero1 = new Quintero("20304050607", "Nombre Quintero 1", "Apellido Quintero 1", "Rubro Quintero 1");
    }

    @Test
    public void agregarQuintero() throws QuinteroYaExisteException {
        repositorio.agregar(quintero1);
        List<Quintero> quinteros = repositorio.obtenerTodos();
        assertEquals(1, quinteros.size());
        assertEquals(quintero1, quinteros.get(0));
    }

    @Test
    public void agregarQuinteroYaExiste() {
        try {
            repositorio.agregar(quintero1);
            repositorio.agregar(quintero1);
            fail("Se esperaba una QuinteroYaExisteException");
        } catch (QuinteroYaExisteException e) {
            assertEquals("el quintero ya esta en la lista", e.getMessage());
        }
    }

    @Test
    public void obtenerPorCuit() throws QuinteroNoEncontradoException, QuinteroYaExisteException {
        repositorio.agregar(quintero1);
        Quintero quinteroEncontrado = repositorio.obtenerPorCuit(quintero1.getCuit());
        assertEquals(quintero1, quinteroEncontrado);
    }

    @Test
    public void obtenerPorCuitNoEncontrado() {
        try {
            repositorio.obtenerPorCuit("99999999999");
            fail("Se esperaba una QuinteroNoEncontradoException");
        } catch (QuinteroNoEncontradoException e) {
            assertEquals("No se encontró un quintero con el CUIT especificado.", e.getMessage());
        }
    }

    @Test
    public void actualizarQuintero() throws QuinteroNoEncontradoException, QuinteroYaExisteException {
        repositorio.agregar(quintero1);
        Quintero quinteroActualizado = new Quintero("20304050607", "Nuevo Nombre Quintero", "Nuevo Apellido Quintero",
                "Nuevo Rubro Quintero");
        repositorio.actualizar(quintero1.getCuit(), quinteroActualizado);
        assertEquals(quinteroActualizado, repositorio.obtenerPorCuit(quintero1.getCuit()));
    }

    @Test
    public void actualizarQuinteroNoEncontrado() {
        try {
            Quintero quinteroActualizado = new Quintero("99999999999", "Nuevo Nombre Quintero",
                    "Nuevo Apellido Quintero", "Nuevo Rubro Quintero");
            repositorio.actualizar("99999999999", quinteroActualizado);
            fail("Se esperaba una QuinteroNoEncontradoException");
        } catch (QuinteroNoEncontradoException e) {
            assertEquals("No se encontró un quintero con el CUIT especificado.", e.getMessage());
        }
    }

    @Test
    public void eliminarQuintero() throws QuinteroNoEncontradoException, QuinteroYaExisteException {
        repositorio.agregar(quintero1);
        repositorio.eliminar(quintero1.getCuit());
        assertEquals(0, repositorio.obtenerTodos().size());
    }

    @Test
    public void eliminarQuinteroNoEncontrado() {
        try {
            repositorio.eliminar("99999999999");
            fail("Se esperaba una QuinteroNoEncontradoException");
        } catch (QuinteroNoEncontradoException e) {
            assertEquals("No se encontró un quintero con el CUIT especificado.", e.getMessage());
        }
    }
}