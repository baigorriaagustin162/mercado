package paquetemercado.ClienteTest;

import paquetemercado.cliente.Empresa;
import org.junit.Assert;
import org.junit.Test;

public class EmpresaTest {
    @Test 
    public void testEmpresasIguales(){
        boolean confirmar;
        Empresa empresa1 = new Empresa("124834309", "PEPSI","Bebidas, alimentos y snacks", "PepsiCo");
        Empresa empresa2 = new Empresa("124834309", "COCA COLA","Bebidas", "Coca-Cola FEMSA, S.A.B. de C.V.");
        confirmar = empresa1.equals(empresa2); // devuelve true porque tienen el mismo cuit, se consideran iguales
        Assert.assertEquals(true, confirmar);
    }

    @Test
    public void testEmpresasDistintas(){
        boolean confirmar;
        Empresa empresa1 = new Empresa("124834309", "PEPSI","Bebidas, alimentos y snacks", "PepsiCo");
        Empresa empresa2 = new Empresa("326473437", "COCA COLA","Bebidas", "Coca-Cola FEMSA, S.A.B. de C.V.");
        confirmar = empresa1.equals(empresa2); // devuelve false porque tienen distinto cuit, se consideran distintos
        Assert.assertEquals(false, confirmar);
    }
}
