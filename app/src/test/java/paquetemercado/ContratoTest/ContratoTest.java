package paquetemercado.ContratoTest;

import paquetemercado.Sector.*;
import paquetemercado.contrato.*;
import paquetemercado.cliente.*;

import java.time.LocalDate;

import org.junit.Assert;
import org.junit.Test;

public class ContratoTest {

    @Test
    public void contratosIgualesPuestosYFechaInicio() {
        boolean confirmacion;
        Quintero quintero1 = new Quintero("37293842382", "Carlos", "Ramirez", "Comercial");
        Puesto puesto3 = new Puesto("Zona sur, n° 3", 26, true, false, 32574, true);
        Contrato contrato1 = new Contrato(LocalDate.of(2024, 10, 10),LocalDate.of(2026,10,10),"BIANCHI SA","Miranda Lucia");
        contrato1.setClienteYPuestoAContrato(quintero1, puesto3); // el quintero1 alquila el puesto3 en el contrato1

        Empresa empresa1 = new Empresa("124834309", "PEPSI", "Bebidas, alimentos y snacks", "PepsiCo");
        Contrato contrato2 = new Contrato(LocalDate.of(2024, 10, 10),LocalDate.of(2026,10,10),"BIANCHI SA","Rios Joaquin");
        contrato2.setClienteYPuestoAContrato(empresa1, puesto3); // la empresa1 alquila el puesto 3 en el contrato2
        confirmacion = contrato1.equals(contrato2); // devuelve true porque se intento alquilar el mismo puesto
        Assert.assertEquals(true, confirmacion);
    }

    @Test
    public void constratosConDistintosPuestos() {
        boolean confirmacion;
        Quintero quintero1 = new Quintero("37293842382", "Carlos", "Ramirez", "Comercial");
        Puesto puesto3 = new Puesto("Zona sur, n° 3", 26, true, false, 32574, true);
        Contrato contrato1 = new Contrato(LocalDate.of(2024, 10, 10),LocalDate.of(2026,10,10),"BIANCHI SA","Miranda Lucia");
        contrato1.setClienteYPuestoAContrato(quintero1, puesto3); // el quintero1 alquila el puesto3 en el contrato 1

        Puesto puesto7 = new Puesto("Zona Norte, n° 3", 20, true, true, 40174, true);
        Contrato contrato2 = new Contrato(LocalDate.of(2024, 9, 10),LocalDate.of(2026,10,10),"BIANCHI SA","Rios Joaquin");
        contrato2.setClienteYPuestoAContrato(quintero1, puesto7); // quintero1 alquila puesto 7 en contrato2
        confirmacion = contrato1.equals(contrato2); // devuelve false porque por mas que sea el mismo quintero, éste
                                                    // puede alquilar varios puestos

        Assert.assertEquals(false, confirmacion);
    }

    @Test
    public void contratosMismoPuestoConDistintaFechaInicio(){
        boolean confirmacion;
        Quintero quintero1 = new Quintero("37293842382", "Carlos", "Ramirez", "Comercial");
        Puesto puesto3 = new Puesto("Zona sur, n° 3", 26, true, false, 32574, true);
        Contrato contrato1 = new Contrato(LocalDate.of(2024, 10, 10),LocalDate.of(2026,10,10),"BIANCHI SA","Miranda Lucia");
        contrato1.setClienteYPuestoAContrato(quintero1, puesto3); // el quintero1 alquila el puesto3 en octubre 2024

        Empresa empresa1 = new Empresa("124834309", "PEPSI", "Bebidas, alimentos y snacks", "PepsiCo");
        Contrato contrato2 = new Contrato(LocalDate.of(2026, 10, 11),LocalDate.of(2028,10,10),"BIANCHI SA","Rios Joaquin");
        contrato2.setClienteYPuestoAContrato(empresa1, puesto3); // la empresa1 alquila el puesto 3 en octubre 2026
        confirmacion = contrato1.equals(contrato2); // devuelve false porque alquila el mismo puesto en fechas distintas
        Assert.assertEquals(false, confirmacion);
    }
}
