package paquetemercado.ContratoTest;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import paquetemercado.contrato.*;
import paquetemercado.Sector.Puesto;
import paquetemercado.cliente.*;
import paquetemercado.contrato.ExceptionsContrato.*;

public class LibreriaContratosTest {
    @Test
    public void ingresarContratoAlRepositorio() throws ContratoVigente, NoHayContratoConEsePuesto {
        Puesto puesto1 = new Puesto("3", 20, false, true, 10200, true);
        Empresa empresa1 = new Empresa("29485748379", "COCA COLA SRL", "BEBIDAS SIN ALCOHOL", "SRL");
        Contrato contrato1 = new Contrato(LocalDate.of(2024, 02, 13),LocalDate.of(2026, 02, 13),"MIRANDA MARCOS","PIERCE VICTOR");
        contrato1.setClienteYPuestoAContrato(empresa1, puesto1);
        LibreriaDeContratos repo1 = new LibreriaDeContratos();
        repo1.agregarContratoALibreria(contrato1);
        boolean confirmar = repo1.siExisteContratoEnRepositorio(contrato1);
        Assert.assertEquals(true, confirmar);
    }

    @Test
    public void eliminarContratoDelRepositorio() throws ContratoVigente, SinContratoEncontrado{
        Puesto puesto1 = new Puesto("3", 20, false, true, 10200, true);
        Empresa empresa1 = new Empresa("29485748379", "COCA COLA SRL", "BEBIDAS SIN ALCOHOL", "SRL");
        Contrato contrato1 = new Contrato(LocalDate.of(2024, 02, 13),LocalDate.of(2026, 02, 13),"MIRANDA MARCOS","PIERCE VICTOR");
        LibreriaDeContratos repo1 = new LibreriaDeContratos();
        // lleno info de contrato
        contrato1.setClienteYPuestoAContrato(empresa1, puesto1);
        repo1.agregarContratoALibreria(contrato1);
        repo1.eliminarContratoDeLibreria(contrato1);
        boolean existeContrato = repo1.siExisteContratoEnRepositorio(contrato1);
        Assert.assertEquals(false, existeContrato);
    }

    @Test
    public void devolverTodosLosContratos() throws ContratoVigente, NoHayContratoConEsePuesto{
        LibreriaDeContratos repo1 = new LibreriaDeContratos();
        Puesto puesto1 = new Puesto("3", 20, false, true, 10200, true);
        Puesto puesto2 = new Puesto("7", 25, true, true, 20200, true);
        Puesto puesto3 = new Puesto("1", 10, true, true, 14200, true);
        Empresa empresa1 = new Empresa("29485748379", "COCA COLA SRL", "BEBIDAS SIN ALCOHOL", "SRL");
        //creando contratos
        Contrato contrato1 = new Contrato(LocalDate.of(2024,2,5),LocalDate.of(2024,2,5),"MIRANDA MARCOS","GIMENEZ PABLO");
        Contrato contrato2 = new Contrato(LocalDate.of(2023,10,15),LocalDate.of(2023,10,15),"MIRANDA MARCOS","FRIAS BRUNO");
        Contrato contrato3 = new Contrato(LocalDate.of(2024,12,2),LocalDate.of(2024,12,2),"MIRANDA MARCOS","LOPEZ AUGUSTO");
        //llenando info contratos
        contrato1.setClienteYPuestoAContrato(empresa1, puesto3);
        contrato2.setClienteYPuestoAContrato(empresa1, puesto1);
        contrato3.setClienteYPuestoAContrato(empresa1, puesto2);
        //guardando contratos en repo
        repo1.agregarContratoALibreria(contrato1);
        repo1.agregarContratoALibreria(contrato2);
        repo1.agregarContratoALibreria(contrato3);
        ArrayList<Contrato> auxContratos = repo1.getContratos();
        Assert.assertTrue(auxContratos.contains(contrato1));
        Assert.assertTrue(auxContratos.contains(contrato2));
        Assert.assertTrue(auxContratos.contains(contrato3));
    }

    @Test(expected = NoHayContratoConEsePuesto.class)
    public void excepcionNoHayContratoConEsePuesto() throws ContratoVigente, SinContratoEncontrado {
        Puesto puesto1 = new Puesto("3", 20, false, true, 10200, true);
        Puesto puesto2 = new Puesto("6", 15, true, true, 11000, true);
        Empresa empresa1 = new Empresa("29485748379", "COCA COLA SRL", "BEBIDAS SIN ALCOHOL", "SRL");
        Contrato contrato1 = new Contrato(LocalDate.of(2024,12,2),LocalDate.of(2024,12,2),"MIRANDA MARCOS","PIERCE VICTOR");
        LibreriaDeContratos repo1 = new LibreriaDeContratos();
        contrato1.setClienteYPuestoAContrato(empresa1, puesto1);
        repo1.agregarContratoALibreria(contrato1); // ingreso contrato al repo
        // busco contrato segun un puesto distinto al del repo y lanzo excepcion
        repo1.getUltimoContratoDelPuesto(puesto2);
    }

    @Test(expected = NoHayContratoEnEsaFecha.class)
    public void excepcionNoHayContratoEnEsaFechaInicio() throws ContratoVigente, SinContratoEncontrado {
        Puesto puesto1 = new Puesto("3", 20, false, true, 10200, true);
        Empresa empresa1 = new Empresa("29485748379", "COCA COLA SRL", "BEBIDAS SIN ALCOHOL", "SRL");
        Contrato contrato1 = new Contrato(LocalDate.of(2024, 02, 10),LocalDate.of(2024, 02, 10),"MIRANDA MARCOS","PIERCE VICTOR");
        LibreriaDeContratos repo1 = new LibreriaDeContratos();
        contrato1.setClienteYPuestoAContrato(empresa1, puesto1);
        repo1.agregarContratoALibreria(contrato1); // ingreso contrato al repo
        // busco contrato/s segun fecha de inicio incorrecta, lanzo excepcion
        repo1.getContratosPorFechaInicio(LocalDate.of(2024, 01, 10));
    }

    @Test(expected = NoHayContratoEnEsaFecha.class)
    public void excepcionNoHayContratoEnEsaFechaFin() throws ContratoVigente, SinContratoEncontrado {
        Puesto puesto1 = new Puesto("3", 20, false, true, 10200, true);
        Empresa empresa1 = new Empresa("29485748379", "COCA COLA SRL", "BEBIDAS SIN ALCOHOL", "SRL");
        Contrato contrato1 = new Contrato(LocalDate.of(2024, 02, 10),LocalDate.of(2024, 02, 10),"MIRANDA MARCOS","PIERCE VICTOR");
        LibreriaDeContratos repo1 = new LibreriaDeContratos();
        contrato1.setClienteYPuestoAContrato(empresa1, puesto1);
        repo1.agregarContratoALibreria(contrato1); // ingreso contrato al repo
        // busco contrato/s segun fecha fin incorrecta, lanza excepcion
        repo1.getContratosPorFechaFin(LocalDate.of(2024, 03, 10));
    }

    @Test(expected = NoHayContratoConEsteCliente.class)
    public void excepcionNoHayContratoConEsteCliente() throws ContratoVigente, SinContratoEncontrado {
        Puesto puesto1 = new Puesto("3", 20, false, true, 10200, true);
        Empresa empresa1 = new Empresa("29485748379", "COCA COLA SRL", "BEBIDAS SIN ALCOHOL", "SRL");
        Quintero quintero1 = new Quintero("20374483981", "CARLOS", "BIANCHI", "ARTESANIAS");
        Contrato contrato1 = new Contrato(LocalDate.of(2024, 02, 10),LocalDate.of(2024, 02, 10),"MIRANDA MARCOS","PIERCE VICTOR");
        LibreriaDeContratos repo1 = new LibreriaDeContratos();
        contrato1.setClienteYPuestoAContrato(empresa1, puesto1);
        repo1.agregarContratoALibreria(contrato1); // ingreso contrato al repo
        // busco contrato/s segun cliente incorrecto, no tiene contrato
        repo1.getContratosSegunCliente(quintero1);
    }

    @Test(expected = ContratoVigente.class)
    public void excepcionContratoVigente() throws ContratoVigente, NoHayContratoConEsePuesto {
        // Defino elementos para agregar un contrato al repo
        LibreriaDeContratos repo1 = new LibreriaDeContratos();
        Puesto puesto1 = new Puesto("3", 20, false, true, 10200, true);
        Empresa empresa1 = new Empresa("29485748379", "COCA COLA SRL", "BEBIDAS SIN ALCOHOL", "SRL");
        Contrato contrato1 = new Contrato(LocalDate.of(2022, 8, 01),LocalDate.of(2024, 8, 01),"MIRANDA MARCOS","PIERCE VICTOR");
        contrato1.setClienteYPuestoAContrato(empresa1, puesto1);
        repo1.agregarContratoALibreria(contrato1); // ingreso contrato al repo

        // otro cliente intentara alquilar ese puesto pero el contrato esta vigente
        Quintero quintero1 = new Quintero("20374483981", "CARLOS", "BIANCHI", "ARTESANIAS");
        Contrato contrato2 = new Contrato(LocalDate.of(2022, 5, 10),LocalDate.of(2024, 8, 01),"MIRANDA MARCOS","ZINEDINE CHRISTIAN");
        contrato2.setClienteYPuestoAContrato(quintero1, puesto1);
        // lanza excepcion: contrato vigente para el puesto, no se puede subir al repo
        repo1.agregarContratoALibreria(contrato2);
    }

    @Test (expected = SinContratoEncontrado.class)
    public void excepcionSinContratoEncontrado() throws ContratoVigente, SinContratoEncontrado {
        // defino un contrato
        Puesto puesto1 = new Puesto("3", 20, false, true, 10200, true);
        Empresa empresa1 = new Empresa("29485748379", "COCA COLA SRL", "BEBIDAS SIN ALCOHOL", "SRL");
        Contrato contrato1 = new Contrato(LocalDate.of(2022, 10, 06),LocalDate.of(2024, 10, 06),"MIRANDA MARCOS","PIERCE VICTOR");
        LibreriaDeContratos repo1 = new LibreriaDeContratos();
        // lleno la informacion del contrato
        contrato1.setClienteYPuestoAContrato(empresa1, puesto1);
        repo1.agregarContratoALibreria(contrato1); // ingreso contrato al repo

        // defino otro C para eliminarlo del repo, lanza excepcion porque no se almacenó
        Contrato contrato2 = new Contrato(LocalDate.of(2024, 10, 10),LocalDate.of(2026, 10, 10),"MIRANDA MARCOS","FACCI MICAELA");
        contrato2.setClienteYPuestoAContrato(empresa1, puesto1);
        repo1.eliminarContratoDeLibreria(contrato2); // excepcion no se encontró contrato
    }

    @Test
    public void devolverUltimoContratoDelPuesto() throws NoHayContratoConEsePuesto, ContratoVigente {
        Puesto puesto1 = new Puesto("3", 20, false, true, 10200, true);
        Empresa empresa1 = new Empresa("29485748379", "COCA COLA SRL", "BEBIDAS SIN ALCOHOL", "SRL");
        Contrato contrato1 = new Contrato(LocalDate.of(2024, 05, 10),LocalDate.of(2024,10,1),"MIRANDA MARCOS","PIERCE VICTOR");
        LibreriaDeContratos repo1 = new LibreriaDeContratos();
        // lleno info de contrato anterior
        contrato1.setClienteYPuestoAContrato(empresa1, puesto1);
        repo1.agregarContratoALibreria(contrato1); // subo al repo
        // lleno el contrato mas nuevo
        Contrato contrato2 = new Contrato(LocalDate.of(2024, 10, 10),LocalDate.of(2025,1,1),"MIRANDA MARCOS","RIOS LEON");
        contrato2.setClienteYPuestoAContrato(empresa1, puesto1);
        repo1.agregarContratoALibreria(contrato2); // subo al repo
        // recibo el ultimo contrato registrado de ese puesto
        Optional<Contrato> auxContrato = repo1.getUltimoContratoDelPuesto(puesto1);
        Contrato contrato4 = auxContrato.get();
        Assert.assertEquals(contrato2, contrato4);
    }

    @Test
    public void devolverContratosPorFechaInicio() throws ContratoVigente, SinContratoEncontrado {
        Puesto puesto1 = new Puesto("3", 20, false, true, 10200, true);
        Puesto puesto2 = new Puesto("7", 15, true, true, 11200, true);
        // clientes
        Empresa empresa1 = new Empresa("29485748379", "COCA COLA SRL", "BEBIDAS SIN ALCOHOL", "SRL");
        Quintero quintero1 = new Quintero("20473647341", "PABLO", "SUAREZ", "ARTESANIAS");
        // iniciar repositorio
        LibreriaDeContratos repo1 = new LibreriaDeContratos();
        // creando obj contrato
        Contrato contrato1 = new Contrato(LocalDate.of(2024, 1, 10),LocalDate.of(2024, 1, 10),"MIRANDA MARCOS","PIERCE VICTOR");
        Contrato contrato2 = new Contrato(LocalDate.of(2024, 1, 10),LocalDate.of(2024, 1, 10),"MIRANDA MARCOS","PIREZ MATIAS");
        // llenando alguna info de los contratos, fecha inicio
        contrato1.setClienteYPuestoAContrato(quintero1, puesto2);
        contrato2.setClienteYPuestoAContrato(empresa1, puesto1);
        // subiendo contratos al repo
        repo1.agregarContratoALibreria(contrato2);
        repo1.agregarContratoALibreria(contrato1);
        // devolver lista de contratos con esta fecha inicio
        ArrayList<Contrato> auxContratos = repo1.getContratosPorFechaInicio(LocalDate.of(2024, 1, 10));
        Assert.assertTrue(auxContratos.contains(contrato1));
        Assert.assertTrue(auxContratos.contains(contrato2));
    }

    @Test
    public void devolverContratosPorFechaFin() throws ContratoVigente, SinContratoEncontrado {
        LocalDate fechaFin = LocalDate.of(2024, 1, 10);
        Puesto puesto1 = new Puesto("3", 20, false, true, 10200, true);
        Puesto puesto2 = new Puesto("7", 15, true, true, 11200, true);
        // clientes
        Empresa empresa1 = new Empresa("29485748379", "COCA COLA SRL", "BEBIDAS SIN ALCOHOL", "SRL");
        Quintero quintero1 = new Quintero("20473647341", "PABLO", "SUAREZ", "ARTESANIAS");
        // iniciar repositorio
        LibreriaDeContratos repo1 = new LibreriaDeContratos();
        // creando obj contrato
        Contrato contrato1 = new Contrato(LocalDate.of(2022, 1, 10),LocalDate.of(2024, 1, 10),"MIRANDA MARCOS","PIERCE VICTOR");
        Contrato contrato2 = new Contrato(LocalDate.of(2022, 10, 10),LocalDate.of(2024, 1, 10),"MIRANDA MARCOS","PIREZ MATIAS");
        // llenando alguna info de los contratos
        contrato1.setClienteYPuestoAContrato(quintero1, puesto2);
        contrato2.setClienteYPuestoAContrato(empresa1, puesto1);
        // subiendo contratos al repo
        repo1.agregarContratoALibreria(contrato2);
        repo1.agregarContratoALibreria(contrato1);
        // devolver lista de contratos con esta fecha fin
        ArrayList<Contrato> auxContratos = repo1.getContratosPorFechaFin(fechaFin);
        Assert.assertTrue(auxContratos.contains(contrato1));
        Assert.assertTrue(auxContratos.contains(contrato2));
    }

    @Test
    public void devolverContratosSegunCliente() throws ContratoVigente, SinContratoEncontrado {
        Puesto puesto1 = new Puesto("3", 20, false, true, 10200, true);
        Puesto puesto2 = new Puesto("7", 15, true, true, 11200, true);
        Empresa empresa1 = new Empresa("29485748379", "COCA COLA SRL", "BEBIDAS SIN ALCOHOL", "SRL");
        Empresa empresa2 = new Empresa("20473647341", "PEPSI CO", "BEBIDAS SIN ALCOHOL", "SA");
        // instanciando contratos
        Contrato contrato1 = new Contrato(LocalDate.of(2022, 10, 10),LocalDate.of(2025, 1, 10),"MIRANDA MARCOS","PIERCE VICTOR");
        Contrato contrato2 = new Contrato(LocalDate.of(2022, 10, 10),LocalDate.of(2026, 1, 10),"MIRANDA MARCOS","QUIROZ JOAQUIN");
        LibreriaDeContratos repo1 = new LibreriaDeContratos();
        // lleno la informacion de los contratos
        contrato1.setClienteYPuestoAContrato(empresa1, puesto1);
        contrato2.setClienteYPuestoAContrato(empresa2, puesto2);
        repo1.agregarContratoALibreria(contrato1); // ingreso contratos al repo
        repo1.agregarContratoALibreria(contrato2);
        // devolver contratos mandando un cliente
        ArrayList<Contrato> auxContratos = repo1.getContratosSegunCliente(empresa1);
        Assert.assertTrue(auxContratos.contains(contrato1));
        Assert.assertFalse(auxContratos.contains(contrato2));
    }

    @Test 
    public void siExisteContratoEnRepositorio() throws NoHayContratoConEsePuesto, ContratoVigente {
        LibreriaDeContratos repo1 = new LibreriaDeContratos();
        Puesto puesto1 = new Puesto("3", 20, false, true, 10200, true);
        Empresa empresa1 = new Empresa("29485748379", "COCA COLA SRL", "BEBIDAS SIN ALCOHOL", "SRL");
        Contrato contrato1 = new Contrato(LocalDate.of(2024, 05, 20),LocalDate.of(2024, 05, 20),"MIRANDA MARCOS","PIERCE VICTOR");
        contrato1.setClienteYPuestoAContrato(empresa1, puesto1);
        repo1.agregarContratoALibreria(contrato1);
        boolean confirmar = repo1.siExisteContratoEnRepositorio(contrato1);
        Assert.assertTrue(confirmar);
    }
}
