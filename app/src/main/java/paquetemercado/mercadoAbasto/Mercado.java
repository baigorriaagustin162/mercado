package paquetemercado.mercadoAbasto;

import paquetemercado.Sector.*;
import paquetemercado.cliente.ClienteRepositorio;
import paquetemercado.contrato.*;
import paquetemercado.lectura.*;

public class Mercado {
    private RepositorioDeMedidores medidores = new RepositorioDeMedidores();
    private SectorRepositorio sectores = new SectorRepositorio();
    private LibreriaDeContratos contratos = new LibreriaDeContratos();
    private ClienteRepositorio clientes = new ClienteRepositorio();

    private static Mercado mercado;

    private Mercado(){};

    public static Mercado instancia(){
        if(mercado == null)
            mercado = new Mercado();
        return mercado;
    }

    public RepositorioDeMedidores getMedidores() {
        return medidores;
    }

    public SectorRepositorio getSectores() {
        return sectores;
    }
    
    public LibreriaDeContratos getContratos() {
        return contratos;
    }

    public ClienteRepositorio getClientes() {
        return clientes;
    }
}
