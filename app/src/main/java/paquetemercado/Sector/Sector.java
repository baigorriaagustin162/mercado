package paquetemercado.Sector;

import paquetemercado.Sector.ExceptionsSector.*;
import java.util.ArrayList;
import java.util.Optional;

public class Sector {
    private String zonaSector;
    private ArrayList<Puesto> puestos;

    public Sector(String zonaSector) {
        this.zonaSector = zonaSector;
        this.puestos = new ArrayList<>();
    }

    // Getters y setters
    public String getZonaSector() {
        return zonaSector;
    }

    public ArrayList<Puesto> getPuestos() {
        return puestos;
    }

    public void setUbicacionOzonaDelSector(String zona){
        this.zonaSector = zona;
    }

    // Método para buscar si existe un puesto en el sector y lanzar una excepción si
    // no se encuentra
    public Optional<Puesto> getPuestoEnSectorSegunUbicacion(String buscarUbicacion) throws PuestoNoEncontradoException{
        Optional<Puesto> puesto = Optional.empty();
        for (Puesto auxPuesto : puestos) {
            if (auxPuesto.getUbicacion().compareTo(buscarUbicacion)==0) {
                puesto = Optional.of(auxPuesto);
            }
        }
        if(!puesto.isPresent())
            throw new PuestoNoEncontradoException(buscarUbicacion);
        return puesto;
    }

    // Método para agregar un puesto al sector
    public void agregarPuesto(Puesto puesto) throws PuestoYaExisteEnSector {
        if (!buscarSiExistePuestoEnSector(puesto.getUbicacion())) {
            puestos.add(puesto);
        }
        else 
            throw new PuestoYaExisteEnSector();
        
    }

    // Método para eliminar un puesto del sector
    public void eliminarPuesto(Puesto puesto) throws NoExistePuestoConEsaUbicacionEnElSector {
        boolean puestoEliminado = false;
        for (int i = 0; i < puestos.size(); i++) {
            Puesto puestoAux = puestos.get(i);
            if (puestoAux.getUbicacion().equals(puesto.getUbicacion())) {
                puestos.remove(i);
                puestoEliminado = true;
                break;
            }
        }
        if(!puestoEliminado)
            throw new NoExistePuestoConEsaUbicacionEnElSector();
    }


    // Método privado que busca si existe un puesto en el sector
    private boolean buscarSiExistePuestoEnSector(String buscarUbicacion) {
        boolean existePuesto = false;
        for (Puesto puestoAux : puestos) {
            if (puestoAux.getUbicacion().equals(buscarUbicacion)) {
                existePuesto = true;
                break;
            }
        }
        return existePuesto;
    }
    public ArrayList<Puesto> getPuestosDisponibles() throws NoHayPuestosDisponiblesEnElSector {
        ArrayList<Puesto> auxPuestos = new ArrayList<>();
        for (Puesto puesto : puestos) {
            if (puesto.getEstaDisponible())
                auxPuestos.add(puesto);
        }
        if (auxPuestos.isEmpty())
            throw new NoHayPuestosDisponiblesEnElSector();
        return auxPuestos;
    }

    public ArrayList<Puesto> getPuestosConPrecioMenorOIgualAParametro(double precioConsulta) throws  NoHayPuestosConPrecioMenorAEse{
        ArrayList<Puesto> auxPuestos = new ArrayList<>();
        for (Puesto puesto : puestos) {
            if (puesto.getPrecioBase()<=precioConsulta)
                auxPuestos.add(puesto);
        }
        if (auxPuestos.isEmpty())
            throw new NoHayPuestosConPrecioMenorAEse();
        return auxPuestos;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null)
            return false;
        if (!(obj instanceof Sector))
            return false;
        Sector auxSector = (Sector) obj;
        return this.zonaSector.equals(auxSector.getZonaSector());
    }
}
