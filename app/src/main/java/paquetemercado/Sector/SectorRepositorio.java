package paquetemercado.Sector;

import java.util.ArrayList;
import java.util.Optional;
import paquetemercado.Sector.ExceptionsSector.*;

public class SectorRepositorio {
    private ArrayList<Sector> sectores;

    public SectorRepositorio() {
        this.sectores = new ArrayList<>();
    }

    public void ingresarSectorAlRepositorio(Sector sector) throws YaExisteElSectorEnElRepositorio{
        for(Sector auxSector:sectores)
        {
            if(auxSector.equals(sector))
                throw new YaExisteElSectorEnElRepositorio();
        }
        sectores.add(sector);
    }

    public ArrayList<Sector> getSectoresDelRepositorio(){
        return sectores;
    }

    public Sector obtenerSectorPorZona(String zonaSector) throws SectorNoEncontradoException {
        Optional<Sector> sector = sectores.stream()
                .filter(s -> s.getZonaSector().equals(zonaSector))
                .findFirst();
        return sector.orElseThrow(() -> new SectorNoEncontradoException(zonaSector));
    }

    public void modificarUbicacion(Sector sectorAActualizar,String ubicacionSectorActualizado) throws SectorNoEncontradoException {
        for (int i = 0; i < sectores.size(); i++) {
            if (sectores.get(i).equals(sectorAActualizar)) {
                sectores.get(i).setUbicacionOzonaDelSector(ubicacionSectorActualizado);;
                return;
            }
        }
        throw new SectorNoEncontradoException(sectorAActualizar.getZonaSector());
    }

    public void eliminarSectorDelRepositorio(Sector sector) throws SectorNoEncontradoException{
        boolean eliminado = false;
        for (int i = 0; i < sectores.size(); i++) {
            if (sector.equals(sectores.get(i))) {
                sectores.remove(i);
                eliminado = true;
                break;
            }
        }
        if (!eliminado) {
            throw new SectorNoEncontradoException(sector.getZonaSector());
        }
    } 
}