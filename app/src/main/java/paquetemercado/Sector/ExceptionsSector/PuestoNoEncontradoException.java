package paquetemercado.Sector.ExceptionsSector;

public class PuestoNoEncontradoException extends Exception {
    public PuestoNoEncontradoException(String ubicacion) {
        super("No se encontró un puesto en la ubicación: " + ubicacion);
    }
}
