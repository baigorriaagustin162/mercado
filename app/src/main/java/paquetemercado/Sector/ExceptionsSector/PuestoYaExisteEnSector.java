package paquetemercado.Sector.ExceptionsSector;

public class PuestoYaExisteEnSector extends Exception{
    public PuestoYaExisteEnSector(){
        System.out.println("En el sector ya existe un puesto con la misma ubicación.");
    }
}
