package paquetemercado.Sector.ExceptionsSector;

public class SectorNoEncontradoException extends Exception {
    public SectorNoEncontradoException(String zonaSector) {
        super("No se encontró un sector con la zona: " + zonaSector);
    }
}
