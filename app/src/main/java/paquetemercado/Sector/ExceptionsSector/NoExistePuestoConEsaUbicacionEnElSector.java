package paquetemercado.Sector.ExceptionsSector;

public class NoExistePuestoConEsaUbicacionEnElSector extends Exception{
    public NoExistePuestoConEsaUbicacionEnElSector(){
        System.out.println("En el sector no existe un puesto en esa ubicación.");
    }
}
