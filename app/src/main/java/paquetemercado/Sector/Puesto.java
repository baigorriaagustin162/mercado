package paquetemercado.Sector;

import paquetemercado.lectura.Medidor;

public class Puesto {
    private String ubicacion;
    private double tamaño;
    private boolean sitieneTecho;
    private boolean sitieneCamaraRefrigerante;
    private double precioBase;
    private boolean EstaDisponible;
    private Medidor medidor;

    public Puesto(String ubicacion, double tamaño, boolean sitieneTecho, boolean sitieneCamaraRefrigerante, double precioBase, boolean EstaDisponible) {
        this.ubicacion = ubicacion;
        this.tamaño = tamaño;
        this.sitieneTecho = sitieneTecho;
        this.sitieneCamaraRefrigerante = sitieneCamaraRefrigerante;
        this.precioBase = precioBase;
        this.EstaDisponible = EstaDisponible;
        this.medidor = null;
    }

    // Método para establecer el medidor después de crear el objeto Puesto
    public void agregarMedidorAlPuesto(Medidor medidor) {
        this.medidor = medidor;
    }

    // Getters y setters
    public void setMedidor(Medidor medidor) {
        this.medidor = medidor;
    }

    public Medidor getMedidorDelPuesto(){
        return medidor;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public double getTamañoEnMetros() {
        return tamaño;
    }

    public boolean siTieneTecho() {
        return sitieneTecho;
    }

    public boolean siTieneCamaraRefrigerante() {
        return sitieneCamaraRefrigerante;
    }

    public double getPrecioBase() {
        return precioBase;
    }

    public boolean getEstaDisponible() {
        return EstaDisponible;
    }

    public void setEstaDisponible(boolean EstaDisponible) {
        this.EstaDisponible = EstaDisponible;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null)
            return false;
        if (!(obj instanceof Puesto))
            return false;
        // Convertir el objeto a un Puesto para poder comparar sus atributos
        Puesto auxPuesto = (Puesto) obj;
        // Comparar los atributos de los puestos
        if(!this.ubicacion.equals(auxPuesto.getUbicacion()))
            return false;
        return true;
        }
}