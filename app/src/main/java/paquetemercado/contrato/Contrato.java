package paquetemercado.contrato;

import paquetemercado.Sector.Puesto;
import paquetemercado.cliente.*;

import java.time.LocalDate;

public class Contrato {
    private LocalDate fechaInicioAlquiler;
    private LocalDate fechaFinAlquiler;
    private String nameResponsableMercado;
    private String nameResponsableRegistro;
    private Puesto puestoAlquilado;
    private Cliente cliente;

    public Contrato(LocalDate fechaInicioAlquiler,LocalDate fechaFinAlquiler,String nameResponsableMercado,String nameResponsableRegistro) {
        setFechaInicioAlquiler(fechaInicioAlquiler);
        setFechaFinAlquiler(fechaFinAlquiler);
        this.nameResponsableMercado = nameResponsableMercado;
        this.nameResponsableRegistro = nameResponsableRegistro;
    }

    public void setFechaInicioAlquiler(LocalDate fechaIniAlq) {
        this.fechaInicioAlquiler = fechaIniAlq;
    }

    public LocalDate getFechaInicioAlquiler() {
        return fechaInicioAlquiler;
    }

    public void setFechaFinAlquiler(LocalDate fechaFinAlq) {
        this.fechaFinAlquiler = fechaFinAlq;
    }

    public LocalDate getFechaFinAlquiler() {
        return fechaFinAlquiler;
    }

    public String getNombreResponsableRegistro() {
        return nameResponsableRegistro;
    }

    public String getNombreResponsableMercado() {
        return nameResponsableMercado;
    }

    public Puesto getPuestoAlquilado() {
        return puestoAlquilado;
    }

    public Cliente getClienteInquilino() {
        return cliente;
    }

    public void setClienteYPuestoAContrato(Cliente cliente, Puesto puesto) {
        this.cliente = cliente;
        this.puestoAlquilado = puesto;
    }

    @Override
    public boolean equals(Object objeto) {
        if (objeto == null)
            return false;
        if (!(objeto instanceof Contrato))
            return false;
        Contrato auxContrato = (Contrato) objeto;
        if (!this.puestoAlquilado.equals(auxContrato.getPuestoAlquilado())
                || !this.fechaInicioAlquiler.isEqual(auxContrato.getFechaInicioAlquiler())
            )
            return false;
        return true;
    }
}