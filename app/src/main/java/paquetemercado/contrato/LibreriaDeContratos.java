package paquetemercado.contrato;

import paquetemercado.Sector.Puesto;
import paquetemercado.cliente.*;
import paquetemercado.contrato.ExceptionsContrato.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Optional;

public class LibreriaDeContratos {
    private ArrayList<Contrato> contratos;

    public LibreriaDeContratos() {
        this.contratos = new ArrayList<>();
    }

    public void agregarContratoALibreria(Contrato contract) throws ContratoVigente, NoHayContratoConEsePuesto {
        Optional<Contrato> resultadoBusqueda = Optional.empty();
        try {
            resultadoBusqueda = getUltimoContratoDelPuesto(contract.getPuestoAlquilado());
        } catch (NoHayContratoConEsePuesto e) {
            contratos.add(contract);
            return;
        }
        if(resultadoBusqueda.isPresent())
        {
            if(resultadoBusqueda.get().getFechaFinAlquiler().isAfter(contract.getFechaInicioAlquiler()))
            throw new ContratoVigente();
        }
        contratos.add(contract);
    }

    public ArrayList<Contrato> getContratos(){
        return this.contratos;
    }

    public Optional<Contrato> getUltimoContratoDelPuesto(Puesto puesto) throws NoHayContratoConEsePuesto {
        Optional<Contrato> contratoVigente = Optional.empty();

        for (Contrato auxContrato : contratos) {
            if (auxContrato.getPuestoAlquilado().equals(puesto)) {
                if (!contratoVigente.isPresent() || auxContrato.getFechaInicioAlquiler()
                        .isAfter(contratoVigente.get().getFechaInicioAlquiler())) {
                    contratoVigente = Optional.of(auxContrato);
                }
            }
        }
        if (!contratoVigente.isPresent()) {
            throw new NoHayContratoConEsePuesto();
        }
        return contratoVigente;
    }

    public ArrayList<Contrato> getContratosPorFechaInicio(LocalDate fechaParaConsulta) throws NoHayContratoEnEsaFecha {
        ArrayList<Contrato> auxContratos = new ArrayList<>();
        for (Contrato contract : contratos) {
            if (contract.getFechaInicioAlquiler().isEqual(fechaParaConsulta))
                auxContratos.add(contract);
        }
        if (auxContratos.isEmpty())
            throw new NoHayContratoEnEsaFecha(fechaParaConsulta);
        return auxContratos;
    }

    public ArrayList<Contrato> getContratosPorFechaFin(LocalDate fechaParaConsulta) throws NoHayContratoEnEsaFecha {
        ArrayList<Contrato> auxContratos = new ArrayList<>();
        for (Contrato contract : contratos) {
            if (contract.getFechaFinAlquiler().isEqual(fechaParaConsulta))
                auxContratos.add(contract);
        }
        if (auxContratos.isEmpty()) {
            throw new NoHayContratoEnEsaFecha(fechaParaConsulta);
        }
        return auxContratos;
    }

    public ArrayList<Contrato> getContratosSegunCliente(Cliente cliente) throws NoHayContratoConEsteCliente {
        ArrayList<Contrato> auxContratos = new ArrayList<>();
        for (Contrato contract : contratos) {
            if (contract.getClienteInquilino().equals(cliente))
                auxContratos.add(contract);
        }
        if (auxContratos.isEmpty()) {
            throw new NoHayContratoConEsteCliente();
        }
        return auxContratos;
    }

    public void eliminarContratoDeLibreria(Contrato contract) throws SinContratoEncontrado {
        boolean eliminado = false;
        for (int i = 0; i < contratos.size(); i++) {
            if (contract.equals(contratos.get(i))) {
                contratos.remove(i);
                eliminado = true;
                break;
            }
        }
        if (!eliminado) {
            throw new SinContratoEncontrado();
        }
    }
    //metodo para test, devolver true or false
    public boolean siExisteContratoEnRepositorio(Contrato buscar) {
        boolean confirmar = false;
        for (Contrato auxC : contratos) {
            if (auxC.equals(buscar)) {
                confirmar = true;
                break;
            }
        }
        return confirmar;
    }

    @Override
    public boolean equals(Object obj){
        if (obj == null)
            return false;
        if (!(obj instanceof LibreriaDeContratos))
            return false;
        LibreriaDeContratos auxRepo = (LibreriaDeContratos) obj;
        if(!auxRepo.getContratos().equals(contratos))
            return false;
        return true;
    }
}