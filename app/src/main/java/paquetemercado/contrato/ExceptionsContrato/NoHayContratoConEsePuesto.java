package paquetemercado.contrato.ExceptionsContrato;

public class NoHayContratoConEsePuesto extends SinContratoEncontrado{
    public NoHayContratoConEsePuesto(){
        System.out.println("No existe contrato que registre el alquiler de este puesto.");
    }
}
