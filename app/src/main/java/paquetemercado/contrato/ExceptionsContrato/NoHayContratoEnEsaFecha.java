package paquetemercado.contrato.ExceptionsContrato;

import java.time.LocalDate;

public class NoHayContratoEnEsaFecha extends SinContratoEncontrado{
    public NoHayContratoEnEsaFecha(LocalDate fecha){
        System.out.println("No existe contrato con la fecha " + fecha);
    }
}
