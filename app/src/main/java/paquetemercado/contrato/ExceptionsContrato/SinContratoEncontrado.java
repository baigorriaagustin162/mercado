package paquetemercado.contrato.ExceptionsContrato;

public class SinContratoEncontrado extends Exception{
    public SinContratoEncontrado(){
        System.out.println("No se encontró contrato en el repositorio con esa información.");
    }
}