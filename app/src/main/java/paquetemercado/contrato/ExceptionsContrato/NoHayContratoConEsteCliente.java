package paquetemercado.contrato.ExceptionsContrato;

public class NoHayContratoConEsteCliente extends SinContratoEncontrado{
    public NoHayContratoConEsteCliente(){
        System.out.println("No se encuentra registro de alquiler de éste cliente en un contrato");
    }
}
