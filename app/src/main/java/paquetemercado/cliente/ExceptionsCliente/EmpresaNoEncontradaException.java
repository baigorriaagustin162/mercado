package paquetemercado.cliente.ExceptionsCliente;

public class EmpresaNoEncontradaException extends Exception {
    public EmpresaNoEncontradaException(String mensaje) {
        super(mensaje);
    }
}
