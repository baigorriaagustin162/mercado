package paquetemercado.cliente.ExceptionsCliente;

public class MensajesExcepcionesEmpresa {
    public static final String EMPRESA_NO_ENCONTRADA = "No se encontró una empresa con el CUIT especificado.";
    public static final String EMPRESA_YA_EXISTE = "La empresa ya existe en la lista";

}