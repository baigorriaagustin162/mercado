package paquetemercado.cliente.ExceptionsCliente;

public class QuinteroYaExisteException extends Exception {
    public QuinteroYaExisteException(String mensaje) {
        super(mensaje);
    }
}
