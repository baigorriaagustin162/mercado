package paquetemercado.cliente.ExceptionsCliente;

public class QuinteroNoEncontradoException extends Exception {
    public QuinteroNoEncontradoException(String mensaje) {
        super(mensaje);
    }
}
