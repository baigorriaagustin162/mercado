package paquetemercado.cliente.ExceptionsCliente;

public class EmpresaYaExisteException extends Exception {
    public EmpresaYaExisteException(String mensaje) {
        super(mensaje);
    }
}
