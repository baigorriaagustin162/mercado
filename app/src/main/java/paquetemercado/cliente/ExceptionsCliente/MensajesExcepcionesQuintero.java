package paquetemercado.cliente.ExceptionsCliente;

public class MensajesExcepcionesQuintero {
    public static final String QUINTERO_NO_ENCONTRADO = "No se encontró un quintero con el CUIT especificado.";
    public static final String QUINTERO_YA_EXISTE = "el quintero ya esta en la lista";
}
