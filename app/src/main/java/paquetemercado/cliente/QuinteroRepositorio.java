package paquetemercado.cliente;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import paquetemercado.cliente.ExceptionsCliente.*;

public class QuinteroRepositorio {
    private List<Quintero> quinteros;

    // Constructor para inicializar la lista de quinteros
    public QuinteroRepositorio() {
        this.quinteros = new ArrayList<>();
    }

    // Método para agregar un quintero a la lista
    public void agregar(Quintero quintero) throws QuinteroYaExisteException {
        Optional<Quintero> quinteroExistente = quinteros.stream()
                .filter(q -> q.getCuit().equals(quintero.getCuit()))
                .findFirst();
        if (quinteroExistente.isPresent()) {
            throw new QuinteroYaExisteException(MensajesExcepcionesQuintero.QUINTERO_YA_EXISTE);
        }
        quinteros.add(quintero);
    }

    // Método para obtener un quintero por su número de CUIT
    public Quintero obtenerPorCuit(String cuit) throws QuinteroNoEncontradoException {
        Optional<Quintero> quintero = quinteros.stream()
                .filter(q -> q.getCuit().equals(cuit))
                .findFirst();
        if (quintero.isPresent()) {
            return quintero.get();
        } else {
            throw new QuinteroNoEncontradoException(MensajesExcepcionesQuintero.QUINTERO_NO_ENCONTRADO);
        }
    }

    // Método para obtener todos los quinteros
    public List<Quintero> obtenerTodos() {
        return new ArrayList<>(quinteros);
    }

    // Método para actualizar un quintero en la lista
    public void actualizar(String cuit, Quintero quinteroActualizado) throws QuinteroNoEncontradoException {
        boolean encontrado = false;
        for (int i = 0; i < quinteros.size(); i++) {
            if (quinteros.get(i).getCuit().equals(cuit)) {
                quinteros.set(i, quinteroActualizado);
                encontrado = true;
                break;
            }
        }
        if (!encontrado) {
            throw new QuinteroNoEncontradoException(MensajesExcepcionesQuintero.QUINTERO_NO_ENCONTRADO);
        }
    }

    // Método para eliminar un quintero de la lista por su número de CUIT
    public void eliminar(String cuit) throws QuinteroNoEncontradoException {
        boolean eliminado = quinteros.removeIf(q -> q.getCuit().equals(cuit));
        if (!eliminado) {
            throw new QuinteroNoEncontradoException(MensajesExcepcionesQuintero.QUINTERO_NO_ENCONTRADO);
        }
    }
}