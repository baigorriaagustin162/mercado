package paquetemercado.cliente;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import paquetemercado.cliente.ExceptionsCliente.*;

public class ClienteRepositorio {
    // Lista para almacenar clientes
    private List<Cliente> clientes;

    // Constructor para inicializar la lista de clientes
    public ClienteRepositorio() {
        this.clientes = new ArrayList<>();
    }

    // Método para agregar un cliente a la lista
    public void agregar(Cliente cliente) throws ClienteYaExisteException {
        Optional<Cliente> clienteExistente = clientes.stream()
                .filter(c -> c.getCuit().equals(cliente.getCuit()))
                .findFirst();
        if (clienteExistente.isPresent()) {
            throw new ClienteYaExisteException(MensajesExcepcionesCliente.CLIENTE_YA_EXISTE);
        }
        clientes.add(cliente);
    }

    // Método para obtener un cliente por su número de CUIT
    public Cliente obtenerPorCuit(String cuit) throws ClienteNoEncontradoException {
        Optional<Cliente> cliente = clientes.stream()
                .filter(c -> c.getCuit().equals(cuit))
                .findFirst();
        if (cliente.isPresent()) {
            return cliente.get();
        } else {
            throw new ClienteNoEncontradoException(MensajesExcepcionesCliente.CLIENTE_NO_ENCONTRADO);
        }
    }

    // Método para obtener todos los clientes
    public List<Cliente> obtenerTodos() {
        return new ArrayList<>(clientes);
    }

    // Método para actualizar un cliente en la lista
    public void actualizar(String cuit, Cliente clienteActualizado) throws ClienteNoEncontradoException {
        boolean encontrado = false;
        for (int i = 0; i < clientes.size(); i++) {
            if (clientes.get(i).getCuit().equals(cuit)) {
                clientes.set(i, clienteActualizado);
                encontrado = true;
                break;
            }
        }
        if (!encontrado) {
            throw new ClienteNoEncontradoException(MensajesExcepcionesCliente.CLIENTE_NO_ENCONTRADO);
        }
    }

    // Método para eliminar un cliente de la lista por su número de CUIT
    public void eliminar(String cuit) throws ClienteNoEncontradoException {
        boolean eliminado = clientes.removeIf(c -> c.getCuit().equals(cuit));
        if (!eliminado) {
            throw new ClienteNoEncontradoException(MensajesExcepcionesCliente.CLIENTE_NO_ENCONTRADO);
        }
    }
}
