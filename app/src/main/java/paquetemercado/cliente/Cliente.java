package paquetemercado.cliente;

public abstract class Cliente {
    protected String cuit;
    protected String nombre;
    protected String rubro;

    public Cliente(String cuit, String nombrePersonaONombreComercial, String rubro) {
        this.cuit = cuit;
        this.nombre = nombrePersonaONombreComercial;
        this.rubro = rubro;
    }

    public String getCuit() {
        return cuit;
    }

    public String getNombreCliente() {
        return nombre;
    }

    public String getRubroCliente() {
        return rubro;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null)
            return false;
        if (!(obj instanceof Cliente))
            return false;
        Cliente auxcliente = (Cliente) obj;
        if(!getCuit().equals(auxcliente.getCuit()))
            return false;
        return true;
    }
}
