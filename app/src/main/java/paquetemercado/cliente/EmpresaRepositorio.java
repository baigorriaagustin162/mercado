package paquetemercado.cliente;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import paquetemercado.cliente.ExceptionsCliente.*;

public class EmpresaRepositorio {
    private List<Empresa> empresas;

    // Constructor para inicializar la lista de empresas
    public EmpresaRepositorio() {
        this.empresas = new ArrayList<>();
    }

    // Método para agregar una empresa a la lista
    public void agregar(Empresa empresa) throws EmpresaYaExisteException {
        Optional<Empresa> empresaExistente = empresas.stream()
                .filter(e -> e.getCuit().equals(empresa.getCuit()))
                .findFirst();
        if (empresaExistente.isPresent()) {
            throw new EmpresaYaExisteException(MensajesExcepcionesEmpresa.EMPRESA_YA_EXISTE);
        }
        empresas.add(empresa);
    }

    // Método para obtener una empresa por su número de CUIT
    public Empresa obtenerPorCuit(String cuit) throws EmpresaNoEncontradaException {
        Optional<Empresa> empresa = empresas.stream()
                .filter(e -> e.getCuit().equals(cuit))
                .findFirst();
        if (empresa.isPresent()) {
            return empresa.get();
        } else {
            throw new EmpresaNoEncontradaException(MensajesExcepcionesEmpresa.EMPRESA_NO_ENCONTRADA);
        }
    }

    // Método para obtener todas las empresas
    public List<Empresa> obtenerTodas() {
        return new ArrayList<>(empresas);
    }

    // Método para actualizar una empresa en la lista
    public void actualizar(String cuit, Empresa empresaActualizada) throws EmpresaNoEncontradaException {
        boolean encontrado = false;
        for (int i = 0; i < empresas.size(); i++) {
            if (empresas.get(i).getCuit().equals(cuit)) {
                empresas.set(i, empresaActualizada);
                encontrado = true;
                break;
            }
        }
        if (!encontrado) {
            throw new EmpresaNoEncontradaException(MensajesExcepcionesEmpresa.EMPRESA_NO_ENCONTRADA);
        }
    }

    // Método para eliminar una empresa de la lista por su número de CUIT
    public void eliminar(String cuit) throws EmpresaNoEncontradaException {
        boolean eliminado = empresas.removeIf(e -> e.getCuit().equals(cuit));
        if (!eliminado) {
            throw new EmpresaNoEncontradaException(MensajesExcepcionesEmpresa.EMPRESA_NO_ENCONTRADA);
        }
    }
}
