package paquetemercado.cliente;

public class Quintero extends Cliente {
    private String apellido;

    public Quintero(String cuit, String nombre, String apellido, String rubro){
        super(cuit, nombre, rubro);
        this.apellido = apellido;
    }

    // Getters para devolver a los atributos privados
    public String getApellido() {
        return apellido;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null)
            return false;
        if (!(obj instanceof Quintero))
            return false;
        Quintero auxQuintero = (Quintero) obj;
        if(!getCuit().equals(auxQuintero.getCuit()))
            return false;
        return true;
    }
}