package paquetemercado.cliente;

public class Empresa extends Cliente {
    private String razonSocial;

    public Empresa(String cuit, String nombreComercial, String rubro, String razonSocial){
        super(cuit,nombreComercial,rubro);
        this.razonSocial = razonSocial;
    }

    // Getters para acceder a los atributos privados
    public void setRazonSocialEmpresa(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getRazonSocialEmpresa() {
        return razonSocial;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null)
            return false;
        if (!(obj instanceof Empresa))
            return false;
        Empresa auxEmpresa = (Empresa) obj;
        if(!getCuit().equals(auxEmpresa.getCuit()))
            return false;
        return true;
    }
}