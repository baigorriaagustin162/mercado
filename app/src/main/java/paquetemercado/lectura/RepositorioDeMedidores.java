package paquetemercado.lectura;

import java.util.ArrayList;
import java.util.Optional;

import paquetemercado.lectura.ExcepcionesMedidorYLectura.NoExisteMedidor;
import paquetemercado.lectura.ExcepcionesMedidorYLectura.YaExisteMedidorEnRepositorio;

public class RepositorioDeMedidores {
    private ArrayList<Medidor> medidores;

    public RepositorioDeMedidores(){
        this.medidores = new ArrayList<>();
    }

    public void ingresarMedidorAlRepositorio(Medidor medidor) throws YaExisteMedidorEnRepositorio{
        for(Medidor auxMedidor:medidores)
        {
            if(auxMedidor.equals(medidor))
                throw new YaExisteMedidorEnRepositorio();
        }
        medidores.add(medidor);
    }

    public void eliminarMedidorDelRepositorio(Medidor medidor) throws NoExisteMedidor{
        boolean eliminado = false;
        for (int i = 0; i < medidores.size(); i++) {
            if (medidor.equals(medidores.get(i))) {
                medidores.remove(i);
                eliminado = true;
                break;
            }
        }
        if (!eliminado) {
            throw new NoExisteMedidor();
        }
    }

    public Optional<Medidor> getMedidorSegunCodigoMedidor(Integer codigoMedidor) throws NoExisteMedidor{
        Optional<Medidor> medidor = Optional.empty();
        for (Medidor auxMediddor : medidores) {
            if (auxMediddor.getCodigoMedidor().equals(codigoMedidor)) {
                medidor = Optional.of(auxMediddor);
            }
        }
        if(!medidor.isPresent())
            throw new NoExisteMedidor();
        return medidor;
    }

    public ArrayList<Medidor> getMedidores(){
        return medidores;
    }
}
