package paquetemercado.lectura;

import java.util.ArrayList;
import java.util.Optional;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import paquetemercado.lectura.ExcepcionesMedidorYLectura.*;

public class Medidor {

    private Integer codigoMedidor;
    private ArrayList<LecturaMedidor> lecturas;

    public Medidor(Integer codMedidor) throws CodigoInvalido{
        setCodigoMedidor(codMedidor);
        this.lecturas = new ArrayList<>();
    }

    private void setCodigoMedidor(Integer codMedidor) throws CodigoInvalido{
        if (codMedidor >= 0)
            this.codigoMedidor = codMedidor;
        else
            throw new CodigoInvalido();
    }

    public Integer getCodigoMedidor() {
        return codigoMedidor;
    }

    public ArrayList<LecturaMedidor> getLecturasDelMedidor() {
        return lecturas;
    }

    public boolean buscarSiExisteLecturaEnMedidor(LecturaMedidor buscar) {
        boolean existeLectura = false;
        for (LecturaMedidor lecturaAux : lecturas) {
            if (lecturaAux.equals(buscar)) {
                existeLectura = true;
                break;
            }
        }
        return existeLectura; // devuelve true si encuentra una lectura con igual fecha en la lista
    }

    public void agregarLecturaDelMedidor(LecturaMedidor lectura) throws NoHayLecturaEnEsteMedidor, YaExisteLecturaDelMesProporsionado{
        Optional<LecturaMedidor> resultadoBusqueda = Optional.empty();
        try {
            resultadoBusqueda = getUltimaLectura();
        } catch (NoHayLecturaEnEsteMedidor e) {
            lecturas.add(lectura);
            return;
        }
        if(resultadoBusqueda.isPresent())
        {
            Integer diferenciaEnMeses = (int) resultadoBusqueda.get().getFechaLectura().until(lectura.getFechaLectura(),ChronoUnit.MONTHS);
            if(diferenciaEnMeses < 1)
                throw new YaExisteLecturaDelMesProporsionado();
        }
        lecturas.add(lectura);
    }

    public void eliminarUnaLecturaDelMedidor(LecturaMedidor buscar) throws NoHayLecturaEnEsteMedidor {
        boolean eliminado = false;
        for (int i = 0; i < lecturas.size(); i++) {
            LecturaMedidor lecturaAux = lecturas.get(i);
            if (lecturaAux.equals(buscar)) {
                lecturas.remove(i);
                eliminado = true;
                break;                        
            }
        }
        if(!eliminado)
            throw new NoHayLecturaEnEsteMedidor();
    }

    public Optional<LecturaMedidor> getUltimaLectura() throws NoHayLecturaEnEsteMedidor{
        Optional<LecturaMedidor> lectura = Optional.empty();
        for (LecturaMedidor auxLectura : lecturas) {
            if (!lectura.isPresent() || auxLectura.getFechaLectura().isAfter(lectura.get().getFechaLectura())) {
                lectura = Optional.of(auxLectura);
            }
        }
        if(!lectura.isPresent())
            throw new NoHayLecturaEnEsteMedidor();
        return lectura;
    }

    public Optional<LecturaMedidor> getLecturaMedidorSegunFecha(LocalDate fecha) throws NoHayLecturaConEsaFechaEnEsteMedidor{
        Optional<LecturaMedidor> lectura = Optional.empty();
        for (LecturaMedidor auxLectura : lecturas) {
            if (auxLectura.getFechaLectura().isEqual(fecha)) {
                lectura = Optional.of(auxLectura);
            }
        }
        if(!lectura.isPresent())
            throw new NoHayLecturaConEsaFechaEnEsteMedidor(fecha);
        return lectura;
    }

    @Override
    public boolean equals(Object objeto) {
        if (objeto == null)
            return false;
        if (!(objeto instanceof Medidor))
            return false;
        Medidor auxMedidor = (Medidor) objeto;
        if (!codigoMedidor.equals(auxMedidor.codigoMedidor))
            return false;
        return true;

    }

}