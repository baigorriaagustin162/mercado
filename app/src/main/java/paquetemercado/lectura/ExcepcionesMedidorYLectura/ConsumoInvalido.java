package paquetemercado.lectura.ExcepcionesMedidorYLectura;

public class ConsumoInvalido extends Exception{
    public ConsumoInvalido(){
        System.out.println("Valor de consumo mensual invalido, ingresar numero decimal mayor a 0.");
    }
}
