package paquetemercado.lectura.ExcepcionesMedidorYLectura;

public class YaExisteLecturaDelMesProporsionado extends Exception{
    public YaExisteLecturaDelMesProporsionado(){
        System.out.println("Este medidor ya contiene una lectura de consumo electrico en el mes que desea ingresar. Intente con otra fecha distinta.");
    }
}
