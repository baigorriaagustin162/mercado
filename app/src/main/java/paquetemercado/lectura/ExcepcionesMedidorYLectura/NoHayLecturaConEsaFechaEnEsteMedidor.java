package paquetemercado.lectura.ExcepcionesMedidorYLectura;

import java.time.LocalDate;

public class NoHayLecturaConEsaFechaEnEsteMedidor extends Exception{
    public NoHayLecturaConEsaFechaEnEsteMedidor(LocalDate fecha){
        System.out.println("En este medidor no existe lectura con la fecha " + fecha + ".");
    }
}
