package paquetemercado.lectura.ExcepcionesMedidorYLectura;

public class YaExisteMedidorEnRepositorio extends Exception{
    public YaExisteMedidorEnRepositorio(){
        System.out.println("En el repositorio ya existe un medidor con el mismo código.");
    }
}
