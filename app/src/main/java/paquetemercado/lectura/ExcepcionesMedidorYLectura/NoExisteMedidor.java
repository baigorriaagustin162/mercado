package paquetemercado.lectura.ExcepcionesMedidorYLectura;

public class NoExisteMedidor extends Exception{
    public NoExisteMedidor(){
        System.out.println("No existe este medidor en el repositorio. Intente cargarlo primero o verifique su código.");
    }
}
