package paquetemercado.lectura.ExcepcionesMedidorYLectura;

public class CodigoInvalido extends Exception{
    public CodigoInvalido(){
        System.out.println("El valor que desea asignar como código no corresponde a un valor entero positivo.");
    }
}
