package paquetemercado.lectura;

import java.time.LocalDate;

import paquetemercado.lectura.ExcepcionesMedidorYLectura.ConsumoInvalido;

public class LecturaMedidor {
    private LocalDate fechaDeLectura;
    private double lecturaUltimoConsumo;

    public LecturaMedidor(LocalDate fechaLectura, double consumoDelMes) throws ConsumoInvalido{
        setfechaLectura(fechaLectura);
        setConsumoMensual(consumoDelMes);
    }

    private void setfechaLectura(LocalDate fechaLectura){
        this.fechaDeLectura = fechaLectura;
    }

    public LocalDate getFechaLectura() {
        return fechaDeLectura;
    }

    private void setConsumoMensual(double consumoDelMes) throws ConsumoInvalido{
        if(consumoDelMes > 0)
            this.lecturaUltimoConsumo = consumoDelMes;
        else
            throw new ConsumoInvalido();
    }

    public double getConsumoMensual() {
        return lecturaUltimoConsumo;
    }

    @Override
    public boolean equals(Object objeto) {
        if (objeto == null)
            return false;
        if (!(objeto instanceof LecturaMedidor))
            return false;
        LecturaMedidor otraLectura = (LecturaMedidor) objeto;
        if (!this.fechaDeLectura.equals(otraLectura.fechaDeLectura) || !(Double.compare(otraLectura.lecturaUltimoConsumo, lecturaUltimoConsumo) == 0))
            return false;
        return true; // devuelve true si son iguales
    }
}