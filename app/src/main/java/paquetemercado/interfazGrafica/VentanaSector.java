package paquetemercado.interfazGrafica;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import paquetemercado.Sector.SectorRepositorio;
import paquetemercado.Sector.ExceptionsSector.*;
import paquetemercado.Sector.Puesto;
import paquetemercado.Sector.Sector;
import paquetemercado.lectura.Medidor;
import paquetemercado.lectura.RepositorioDeMedidores;
import paquetemercado.lectura.ExcepcionesMedidorYLectura.NoExisteMedidor;

public class VentanaSector extends JFrame{
    private JTextField letraSectorField;
    private JTextField ubicacionPuestoField;
    private JTextField tamañoField;
    private JCheckBox tieneTechoBox;
    private JCheckBox tieneCamaraBox;
    private JCheckBox estaDisponibleBox;
    private JTextField precioField;
    private JTextField codigoMedidorField;
    private JTextArea listadoTextArea;
    private SectorRepositorio sectorRepositorio;
    private JComboBox<String> sectorComboBox;
    private JComboBox<String> puestoComboBox;
    private RepositorioDeMedidores repositorioDeMedidores;
    private boolean validarCodigoMedidor;
    JFrame ventanaAnterior;
    JTextField buscarUbicacion;
    JTable tablaMostrar;
    private JMenu jMenu1;
    private JMenu jMenu2;
    private JMenu jMenu3;
    private JMenuBar jMenuBar1;
    private JMenuItem jMenuItem1;
    private JMenuItem jMenuItem2;
    private JMenuItem jMenuItem4;
    private JMenuItem jMenuItem5;
    private JMenuItem jMenuItem6;


    public VentanaSector(JFrame ventanaAnterior,SectorRepositorio repositorio, RepositorioDeMedidores repoMedidores) {
        sectorRepositorio = repositorio;
        this.ventanaAnterior = ventanaAnterior;
        repositorioDeMedidores = repoMedidores;
        setTitle("Mercado de Abasto");
        setSize(800, 600);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        validarCodigoMedidor = false; // cambiar valor en caso de querer probar la validacion del medidor o no
        initUI();
    }

    private void initUI() {
        JPanel panelPrincipal = new JPanel(new BorderLayout());

        // Configurar la barra de menú
        jMenuBar1 = new JMenuBar();
        jMenu1 = new JMenu("Buscar");
        jMenuItem1 = new JMenuItem("Puestos Por Sector");
        jMenuItem1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                ventanaBuscarPuestosSegunSector();
            }
        });
        jMenu1.add(jMenuItem1);
        jMenuItem2 = new JMenuItem("Puesto según Precio");
        jMenu1.add(jMenuItem2);
        jMenuBar1.add(jMenu1);

        jMenu2 = new JMenu("Eliminar");
        jMenuItem4 = new JMenuItem("Puesto");
        jMenu2.add(jMenuItem4);
        jMenuItem5 = new JMenuItem("Sector");
        jMenu2.add(jMenuItem5);
        jMenuBar1.add(jMenu2);

        jMenu3 = new JMenu("Ingresar");
        jMenuItem6 = new JMenuItem("Puesto");
        jMenuItem6.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                ventanaIngresarPuesto();
            }
        });
        jMenu3.add(jMenuItem6);
        jMenuBar1.add(jMenu3);

        // Añadir la barra de menú al panel principal
        panelPrincipal.add(jMenuBar1, BorderLayout.NORTH);

        // Crear un JLabel para la imagen y añadirlo al panel principal
        ImageIcon icon = new ImageIcon("imgSector.jpg"); // Asegúrate de que la imagen "zLOGO.png" está en la ruta correcta
        JLabel imagenLabel = new JLabel(icon);
        panelPrincipal.add(imagenLabel, BorderLayout.CENTER);

        // Crear y configurar el área de texto dentro de un JScrollPane
        listadoTextArea = new JTextArea();
        listadoTextArea.setEditable(false);
        JScrollPane scrollPane = new JScrollPane(listadoTextArea);
        panelPrincipal.add(scrollPane, BorderLayout.SOUTH);

        // Crear un panel para el botón "Atrás" y configurar su disposición
        JPanel panelBotonAtras = new JPanel(new FlowLayout(FlowLayout.LEFT));
        JButton bVolver = new JButton("Atrás");
        bVolver.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                ventanaAnterior.setVisible(true);
                dispose();
            }
        });
        panelBotonAtras.add(bVolver);
        panelPrincipal.add(panelBotonAtras, BorderLayout.PAGE_END);

        // Establecer el contenido del frame
        setContentPane(panelPrincipal);
    }

    private void limpiarCampos() {
        letraSectorField.setText("");
        ubicacionPuestoField.setText("");
        tamañoField.setText("");
        tieneTechoBox.setSelected(false);
        tieneCamaraBox.setSelected(false);
        estaDisponibleBox.setSelected(false);
        precioField.setText("");
        codigoMedidorField.setText("");
    }

    private void agregarSectorYPuesto() {
        String letraSector = letraSectorField.getText().trim();
        String ubicacionPuesto = ubicacionPuestoField.getText().trim();
        if (letraSector.isEmpty() || ubicacionPuesto.isEmpty()) {
            mostrarMensaje("Por favor, complete los campos para agregar un sector y un puesto.");
            return;
        }

        // Agregar sector
        Sector sector;
        try {
            sector = sectorRepositorio.obtenerSectorPorZona(letraSector);
        } catch (SectorNoEncontradoException ex) {
            sector = new Sector(letraSector);
            try {
                sectorRepositorio.ingresarSectorAlRepositorio(sector);
            } catch (YaExisteElSectorEnElRepositorio e) {
                mostrarMensaje("El sector ya existe en el repositorio.");
                return;
            }
        }

        // Agregar puesto
        double tamaño;
        try {
            tamaño = Double.parseDouble(tamañoField.getText().trim());
        } catch (NumberFormatException ex) {
            mostrarMensaje("Por favor, ingrese un tamaño válido.");
            return;
        }
        boolean tieneTecho = tieneTechoBox.isSelected();
        boolean tieneCamara = tieneCamaraBox.isSelected();
        boolean estaDisponible = estaDisponibleBox.isSelected();
        double precioBase;
        try {
            precioBase = Double.parseDouble(precioField.getText().trim());
        } catch (NumberFormatException ex) {
            mostrarMensaje("Por favor, ingrese un precio válido.");
            return;
        }

        if (validarCodigoMedidor) {
            String codigoMedidor = codigoMedidorField.getText().trim();
            if (codigoMedidor.isEmpty()) {
                mostrarMensaje("Por favor, ingrese el código del medidor.");
                return;
            }

            try {
                Integer codigoMedidorInt = Integer.parseInt(codigoMedidor);
                Medidor medidor = repositorioDeMedidores.getMedidorSegunCodigoMedidor(codigoMedidorInt)
                        .orElseThrow(() -> new NoExisteMedidor());

                Puesto puesto = new Puesto(ubicacionPuesto, tamaño, tieneTecho, tieneCamara, precioBase,
                        estaDisponible);
                puesto.agregarMedidorAlPuesto(medidor);
                sector.agregarPuesto(puesto);
                mostrarMensaje("Sector y puesto agregados correctamente.");
                actualizarListadoPuestos(sector);
            } catch (NumberFormatException ex) {
                mostrarMensaje("Por favor, ingrese un código de medidor válido.");
            } catch (NoExisteMedidor ex) {
                mostrarMensaje("El medidor con el código ingresado no existe.");
            } catch (PuestoYaExisteEnSector ex) {
                mostrarMensaje("El puesto ya existe en el sector.");
            }
        } else {
            Puesto puesto = new Puesto(ubicacionPuesto, tamaño, tieneTecho, tieneCamara, precioBase, estaDisponible);
            try {
                sector.agregarPuesto(puesto);
                mostrarMensaje("Puesto agregados correctamente.");
                actualizarListadoPuestos(sector);
            } catch (PuestoYaExisteEnSector ex) {
                mostrarMensaje("El puesto ya existe en el sector.");
            }
        }
    }

    private void actualizarListadoPuestos(Sector sector) {
        listadoTextArea.setText("");
        for (Puesto puesto : sector.getPuestos()) {
            listadoTextArea.append("Ubicación: " + puesto.getUbicacion() + ", Tamaño: " + puesto.getTamañoEnMetros()
                    + " m², Techo: " + puesto.siTieneTecho() + ", Cámara: " + puesto.siTieneCamaraRefrigerante()
                    + ", Precio: " + puesto.getPrecioBase() + ", Disponible: " + puesto.getEstaDisponible() + "\n");
        }
    }

    private void verListadoSectoresYPuestos() {
        listadoTextArea.setText("");
        for (Sector sector : sectorRepositorio.getSectoresDelRepositorio()) {
            listadoTextArea.append("Sector: " + sector.getZonaSector() + "\n");
            for (Puesto puesto : sector.getPuestos()) {
                listadoTextArea.append("    Puesto - Ubicación: " + puesto.getUbicacion() + ", Tamaño: "
                        + puesto.getTamañoEnMetros()
                        + " m², Techo: " + puesto.siTieneTecho() + ", Cámara: " + puesto.siTieneCamaraRefrigerante()
                        + ", Precio: " + puesto.getPrecioBase() + ", Disponible: " + puesto.getEstaDisponible() + "\n");
            }
        }
    }

    private void seleccionarPuesto() {
        String sectorSeleccionado = (String) sectorComboBox.getSelectedItem();
        String ubicacionPuestoSeleccionado = (String) puestoComboBox.getSelectedItem();

        if (sectorSeleccionado == null || ubicacionPuestoSeleccionado == null) {
            mostrarMensaje("Por favor, seleccione un sector y un puesto.");
            return;
        }

        try {
            Sector sector = sectorRepositorio.obtenerSectorPorZona(sectorSeleccionado);
            Puesto puesto = sector.getPuestoEnSectorSegunUbicacion(ubicacionPuestoSeleccionado).orElse(null);

            if (puesto != null && puesto.getEstaDisponible()) {
                puesto.setEstaDisponible(false);
                mostrarMensaje("Puesto seleccionado correctamente. Puede proceder a realizar el contrato.");
                actualizarListadoPuestos(sector);
            } else {
                mostrarMensaje("El puesto no está disponible.");
            }
        } catch (SectorNoEncontradoException e) {
            mostrarMensaje("Sector no encontrado.");
        } catch (PuestoNoEncontradoException e) {
            mostrarMensaje("Puesto no encontrado.");
        }
    }

    private void mostrarMensaje(String mensaje) {
        JOptionPane.showMessageDialog(this, mensaje);
    }

    private void actualizarComboBoxSectores() {
        sectorComboBox.removeAllItems();
        for (Sector sector : sectorRepositorio.getSectoresDelRepositorio()) {
            sectorComboBox.addItem(sector.getZonaSector());
        }

        sectorComboBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                actualizarComboBoxPuestos();
            }
        });
    }

    private void actualizarComboBoxPuestos() {
        puestoComboBox.removeAllItems();
        String sectorSeleccionado = (String) sectorComboBox.getSelectedItem();
        if (sectorSeleccionado != null) {
            try {
                Sector sector = sectorRepositorio.obtenerSectorPorZona(sectorSeleccionado);
                for (Puesto puesto : sector.getPuestos()) {
                    if (puesto.getEstaDisponible()) {
                        puestoComboBox.addItem(puesto.getUbicacion());
                    }
                }
            } catch (SectorNoEncontradoException e) {
                mostrarMensaje("Sector no encontrado.");
            }
        }
    }

    private void ventanaIngresarPuesto() {
        dispose();
        JFrame nuevaVentana = new JFrame("Ingresar Puesto");
        nuevaVentana.setSize(800, 600);
        nuevaVentana.setDefaultCloseOperation(EXIT_ON_CLOSE);
        nuevaVentana.setLocationRelativeTo(null);

        JPanel panel = new JPanel(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.insets = new Insets(5, 5, 5, 5);
        gbc.fill = GridBagConstraints.HORIZONTAL;

        // Campos de entrada para Sector
        JLabel letraSectorLabel = new JLabel("Letra/nombre del Sector:");
        letraSectorField = new JTextField(20);
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.anchor = GridBagConstraints.WEST;
        panel.add(letraSectorLabel, gbc);
        gbc.gridx = 1;
        panel.add(letraSectorField, gbc);

        // Campos de entrada para Puesto
        JLabel ubicacionPuestoLabel = new JLabel("Ubicación del Puesto:");
        ubicacionPuestoField = new JTextField(20);
        gbc.gridx = 0;
        gbc.gridy = 1;
        panel.add(ubicacionPuestoLabel, gbc);
        gbc.gridx = 1;
        panel.add(ubicacionPuestoField, gbc);

        JLabel tamañoLabel = new JLabel("Tamaño (m²):");
        tamañoField = new JTextField(20);
        gbc.gridx = 0;
        gbc.gridy = 2;
        panel.add(tamañoLabel, gbc);
        gbc.gridx = 1;
        panel.add(tamañoField, gbc);

        tieneTechoBox = new JCheckBox("Tiene Techo");
        gbc.gridx = 0;
        gbc.gridy = 3;
        panel.add(tieneTechoBox, gbc);

        tieneCamaraBox = new JCheckBox("Tiene Cámara Refrigerante");
        gbc.gridx = 1;
        panel.add(tieneCamaraBox, gbc);

        estaDisponibleBox = new JCheckBox("¿El puesto se encuentra disponible para alquilar?");
        gbc.gridx = 0;
        gbc.gridy = 4;
        gbc.gridwidth = 2;
        panel.add(estaDisponibleBox, gbc);
        gbc.gridwidth = 1;

        JLabel precioLabel = new JLabel("Precio Base:");
        precioField = new JTextField(20);
        gbc.gridx = 0;
        gbc.gridy = 5;
        panel.add(precioLabel, gbc);
        gbc.gridx = 1;
        panel.add(precioField, gbc);

        // Campo de entrada para el medidor
        JLabel codigoMedidorLabel = new JLabel("Código del Medidor:");
        codigoMedidorField = new JTextField(20);
        gbc.gridx = 0;
        gbc.gridy = 6;
        panel.add(codigoMedidorLabel, gbc);
        gbc.gridx = 1;
        panel.add(codigoMedidorField, gbc);

        // Botón para agregar sector/puesto
        JButton agregarSectorPuestoButton = new JButton("Agregar Puesto");
        agregarSectorPuestoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                agregarSectorYPuesto();
                actualizarComboBoxSectores();
                actualizarComboBoxPuestos();
            }
        });
        gbc.gridx = 0;
        gbc.gridy = 7;
        gbc.gridwidth = 2;
        gbc.anchor = GridBagConstraints.CENTER;
        panel.add(agregarSectorPuestoButton, gbc);
        gbc.gridwidth = 1;

        // Botón para ver listado de sectores y puestos
        JButton verListadoButton = new JButton("Listado de Puestos agregados");
        verListadoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                verListadoSectoresYPuestos();
            }
        });
        gbc.gridy = 8;
        panel.add(verListadoButton, gbc);

        // ComboBox para seleccionar sector y puesto
        JLabel seleccionarSectorLabel = new JLabel("Seleccionar Sector:");
        gbc.gridy = 9;
        gbc.gridx = 0;
        panel.add(seleccionarSectorLabel, gbc);
        sectorComboBox = new JComboBox<>();
        gbc.gridx = 1;
        panel.add(sectorComboBox, gbc);

        JLabel seleccionarPuestoLabel = new JLabel("Seleccionar el Puesto que más le sea útil:");
        gbc.gridy = 10;
        gbc.gridx = 0;
        panel.add(seleccionarPuestoLabel, gbc);
        puestoComboBox = new JComboBox<>();
        gbc.gridx = 1;
        panel.add(puestoComboBox, gbc);

        // Área de texto para mostrar la lista de puestos
        listadoTextArea = new JTextArea(10, 30);
        listadoTextArea.setEditable(false);
        JScrollPane scrollPane = new JScrollPane(listadoTextArea);
        gbc.gridy = 11;
        gbc.gridx = 0;
        gbc.gridwidth = 2;
        gbc.gridheight = 2;
        gbc.fill = GridBagConstraints.BOTH;
        panel.add(scrollPane, gbc);
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridwidth = 1;

        // Botón para limpiar los campos
        JButton limpiarButton = new JButton("Limpiar");
        limpiarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                limpiarCampos();
            }
        });
        gbc.gridy = 12;
        gbc.gridx = 6;
        gbc.gridwidth = 1;
        panel.add(limpiarButton, gbc);

        nuevaVentana.add(panel, BorderLayout.CENTER);

        // Crear un nuevo panel para el botón "Seleccionar Puesto" y agregarlo a la derecha
        JPanel panelDerecha = new JPanel();
        panelDerecha.setLayout(new BorderLayout());
        JButton seleccionarPuestoButton = new JButton("Seleccionar Puesto");
        seleccionarPuestoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                seleccionarPuesto();
            }
        });
        panelDerecha.add(seleccionarPuestoButton, BorderLayout.NORTH);

        JButton volverAtras = new JButton("Atrás");
        volverAtras.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ventanaAnterior.setVisible(true);
                nuevaVentana.dispose();
            }
        });
        JPanel panelBotonAtras = new JPanel(new FlowLayout());
        panelBotonAtras.add(volverAtras);
        panelDerecha.add(panelBotonAtras, BorderLayout.SOUTH);
        nuevaVentana.add(panelDerecha, BorderLayout.EAST);

        nuevaVentana.setVisible(true);
    }

    private void ventanaBuscarPuestosSegunSector(){
        JFrame nuevaVentana = new JFrame("Buscar Puestos según Sector");
        nuevaVentana.setSize(800, 600);
        nuevaVentana.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        nuevaVentana.setLocationRelativeTo(null);
        dispose();

        JPanel jPanel1 = new JPanel();
        JLabel jLabel1 = new JLabel("Ubicación Sector:");
        buscarUbicacion = new JTextField(20);
        JScrollPane jScrollPane1 = new JScrollPane();
        tablaMostrar = new JTable();
        JButton bVolver = new JButton("Atrás");
        JButton btBuscar = new JButton("Buscar");

        bVolver.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                ventanaAnterior.setVisible(true);
                nuevaVentana.dispose();
            }
        });

        btBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buscarPuestosEnSectorX();
            }
        });

        tablaMostrar.setModel(new javax.swing.table.DefaultTableModel(
            new Object[][] {},
            new String[] { "SECTOR", "PUESTO", "DISPONIBLE" }
        ));
        jScrollPane1.setViewportView(tablaMostrar);

        GroupLayout jPanel1Layout = new GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(bVolver)
                            .addGap(0, 0, Short.MAX_VALUE))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(jLabel1)
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(buscarUbicacion)
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(btBuscar))
                        .addGroup(GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                            .addGap(0, 10, Short.MAX_VALUE)
                            .addComponent(jScrollPane1, GroupLayout.PREFERRED_SIZE, 700, GroupLayout.PREFERRED_SIZE)
                            .addGap(10, 10, 10)))
                    .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel1)
                        .addComponent(buscarUbicacion, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addComponent(btBuscar))
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 20, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, GroupLayout.PREFERRED_SIZE, 450, GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(bVolver)
                    .addContainerGap())
        );

        GroupLayout layout = new GroupLayout(nuevaVentana.getContentPane());
        nuevaVentana.getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jPanel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jPanel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addContainerGap())
        );

        nuevaVentana.add(jPanel1);
        nuevaVentana.setVisible(true);
    }

    private void buscarPuestosEnSectorX(){
        String ubicacion = buscarUbicacion.getText();
        Sector sector;
        try {
            sector = sectorRepositorio.obtenerSectorPorZona(ubicacion);
            DefaultTableModel model = (DefaultTableModel) tablaMostrar.getModel();
            model.setRowCount(0); // Limpiar la tabla antes de agregar nuevos datos
            for (Puesto puesto : sector.getPuestos()) {
                model.addRow(new Object[]{sector.getZonaSector(), puesto.getUbicacion(), puesto.getEstaDisponible() ? "Sí" : "No"});
            }
        } catch (SectorNoEncontradoException e) {
            mostrarMensaje("Sector no encontrado.");
        }
    }
}
