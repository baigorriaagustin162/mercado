package paquetemercado.interfazGrafica;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import paquetemercado.mercadoAbasto.Mercado;

public class VentanaPrincipal extends JFrame{
    private Mercado mercado = Mercado.instancia();
    private JPanel panelPrincipal;

    public VentanaPrincipal() {
        setSize(500, 500);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setTitle("Mercado de Abasto");
        iniciarComponentes();
    }

    private void iniciarComponentes() {
        panelPrincipal = new JPanel(new GridBagLayout()); // Usamos GridBagLayout en lugar de setLayout(null)
        panelPrincipal.setBackground(Color.WHITE);
        getContentPane().add(panelPrincipal); // Añadimos al content pane, no al frame directamente
        imagenMenu();
        slogan();
        botonIniciar();
    }

    private void imagenMenu() {
        JLabel lblLogo = new JLabel();
        ImageIcon icon = new ImageIcon("zLOGO.png");
        Image image = icon.getImage().getScaledInstance(490, 280, Image.SCALE_SMOOTH);
        ImageIcon scaledIcon = new ImageIcon(image);
        lblLogo.setIcon(scaledIcon);
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.gridwidth = 2; // Ocupa dos columnas
        gbc.fill = GridBagConstraints.HORIZONTAL; // Que ocupe todo el espacio horizontalmente
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.insets = new Insets(0, 0, 10, 0); // Márgenes
        panelPrincipal.add(lblLogo, gbc);
    }

    private void slogan() {
        JLabel eSlogan = new JLabel("Donde la calidad y el ahorro se dan la mano", SwingConstants.CENTER);
        eSlogan.setFont(new Font("Roboto", Font.BOLD, 16));
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.gridwidth = 2; // Ocupa dos columnas
        gbc.fill = GridBagConstraints.HORIZONTAL; // Que ocupe todo el espacio horizontalmente
        gbc.insets = new Insets(0, 10, 10, 10); // Márgenes
        panelPrincipal.add(eSlogan, gbc);
    }

    private void botonIniciar() {
        JButton botonIniciar = new JButton("Comenzar");
        botonIniciar.setFont(new Font("Roboto", 1, 13));

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 2;
        gbc.gridwidth = 1; // Ocupa una columna
        gbc.anchor = GridBagConstraints.CENTER; // Centrado en el centro
        gbc.insets = new Insets(10, 200, 20, 10); // Márgenes

        panelPrincipal.add(botonIniciar, gbc);

        botonIniciar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                InicioMenu menu = new InicioMenu(VentanaPrincipal.this, mercado);
                menu.setVisible(true);
                dispose();
            }
        });
    }
}
