package paquetemercado.interfazGrafica;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import paquetemercado.mercadoAbasto.Mercado;

public class InicioMenu extends JFrame{
    private JPanel panelPrincipal;
    private JFrame ventanaPrincipal;
    private Mercado mercado;

    public InicioMenu(JFrame ventanaPrincipal, Mercado mercado) {
        this.ventanaPrincipal = ventanaPrincipal;
        this.mercado = mercado;

        setSize(500, 500);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setTitle("Menú");

        iniciarComponentes();
    }

    private void iniciarComponentes() {
        panelPrincipal = new JPanel(new GridBagLayout()); // Usamos GridBagLayout en lugar de setLayout(null)
        getContentPane().add(panelPrincipal);
        btCliente();
        btSector();
        btMedidor();
        btContrato();
        bVolverVPrincipal();
    }

    private void btCliente() {
        // Botón de Cliente
        JLabel lblCliente = new JLabel("Cliente", SwingConstants.CENTER);
        lblCliente.setFont(new Font("Roboto", Font.BOLD, 14));
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.gridwidth = 1;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(10, 10, 10, 10); // Márgenes
        panelPrincipal.add(lblCliente, gbc);

        JButton bClientes = new JButton();
        bClientes.setPreferredSize(new Dimension(100, 100)); // Tamaño del botón
        ImageIcon icon = new ImageIcon("zClientes.png");
        Image image = icon.getImage().getScaledInstance(bClientes.getPreferredSize().width, bClientes.getPreferredSize().height, Image.SCALE_SMOOTH);
        ImageIcon scaledIcon = new ImageIcon(image);
        bClientes.setIcon(scaledIcon);
        bClientes.setContentAreaFilled(false);
        bClientes.setHorizontalTextPosition(SwingConstants.CENTER);
        bClientes.setVerticalTextPosition(SwingConstants.BOTTOM);

        gbc.gridy = 1;
        gbc.anchor = GridBagConstraints.CENTER;
        panelPrincipal.add(bClientes, gbc);
    }

    private void btSector() {
        // Botón de Sector
        JLabel lblSector = new JLabel("Sector", SwingConstants.CENTER);
        lblSector.setFont(new Font("Roboto", Font.BOLD, 14));
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 0;
        gbc.gridwidth = 1;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(10, 10, 10, 10);// Márgenes
        panelPrincipal.add(lblSector, gbc);

        JButton bSector = new JButton();
        bSector.setPreferredSize(new Dimension(100, 100)); // Tamaño del botón
        ImageIcon icon = new ImageIcon("zSector.png");
        Image image = icon.getImage().getScaledInstance(80, 80, Image.SCALE_SMOOTH);
        ImageIcon scaledIcon = new ImageIcon(image);
        bSector.setIcon(scaledIcon);
        bSector.setContentAreaFilled(false);
        bSector.setHorizontalTextPosition(SwingConstants.CENTER);
        bSector.setVerticalTextPosition(SwingConstants.BOTTOM);

        gbc.gridy = 1;
        gbc.anchor = GridBagConstraints.CENTER;
        panelPrincipal.add(bSector, gbc);

        //ACCION PARA IR A LA VENTANA DE SECTOR
        ActionListener oyenteVentanaSector = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                VentanaSector ventana = new VentanaSector(InicioMenu.this,mercado.getSectores(),mercado.getMedidores());
                ventana.setVisible(true);
                dispose();
            }
            
        };
        bSector.addActionListener(oyenteVentanaSector);
    }

    private void btMedidor() {
        // Botón de Sector
        JLabel lblMedidor = new JLabel("Medidor", SwingConstants.CENTER);
        lblMedidor.setFont(new Font("Roboto", Font.BOLD, 14));
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 2;
        gbc.gridy = 0;
        gbc.gridwidth = 1;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(10, 10, 10, 10);// Márgenes
        panelPrincipal.add(lblMedidor, gbc);

        JButton bMedidor = new JButton();
        bMedidor.setPreferredSize(new Dimension(100, 100)); // Tamaño del botón
        ImageIcon icon = new ImageIcon("zMedidor.png");
        Image image = icon.getImage().getScaledInstance(75, 75, Image.SCALE_SMOOTH);
        ImageIcon scaledIcon = new ImageIcon(image);
        bMedidor.setIcon(scaledIcon);
        bMedidor.setContentAreaFilled(false);
        bMedidor.setHorizontalTextPosition(SwingConstants.CENTER);
        bMedidor.setVerticalTextPosition(SwingConstants.BOTTOM);

        gbc.gridy = 1;
        gbc.anchor = GridBagConstraints.CENTER;
        panelPrincipal.add(bMedidor, gbc);
    }

    private void btContrato() {
        // Botón de Sector
        JLabel lblContrato = new JLabel("Contrato", SwingConstants.CENTER);
        lblContrato.setFont(new Font("Roboto", Font.BOLD, 14));
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 3;
        gbc.gridy = 0;
        gbc.gridwidth = 1;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(10, 10, 10, 10);// Márgenes
        panelPrincipal.add(lblContrato, gbc);

        JButton bContrato = new JButton();
        bContrato.setPreferredSize(new Dimension(100, 100)); // Tamaño del botón
        ImageIcon icon = new ImageIcon("zContrato.png");
        Image image = icon.getImage().getScaledInstance(100, 100, Image.SCALE_SMOOTH);
        ImageIcon scaledIcon = new ImageIcon(image);
        bContrato.setIcon(scaledIcon);
        bContrato.setContentAreaFilled(false);
        bContrato.setHorizontalTextPosition(SwingConstants.CENTER);
        bContrato.setVerticalTextPosition(SwingConstants.BOTTOM);

        gbc.gridy = 1;
        gbc.anchor = GridBagConstraints.CENTER;
        panelPrincipal.add(bContrato, gbc);
    }

    private void bVolverVPrincipal() {
        JButton bVolver = new JButton("Atrás");
        bVolver.setFont(new Font("Arial", 1, 12));

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = -1;
        gbc.gridy = 2;
        gbc.gridwidth = 2;
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.insets = new Insets(200, -150, 0, 0); // Márgenes

        panelPrincipal.add(bVolver, gbc);

        ActionListener oyenteVolverAtras = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ventanaPrincipal.setVisible(true);
                dispose();
            }
        };
        bVolver.addActionListener(oyenteVolverAtras);
    }
}
